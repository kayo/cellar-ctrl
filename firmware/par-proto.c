#include "par-proto.h"

#include <stddef.h>
#include <string.h>

typedef /*enum _par_proto_op*/ uint8_t par_proto_op_t;
typedef struct _par_proto_head par_proto_head_t;

enum _par_proto_op {
  PAR_PROTO_NONE,
  PAR_PROTO_INFO,
  PAR_PROTO_GETP,
  PAR_PROTO_SETP,
  PAR_PROTO_LOAD,
  PAR_PROTO_SAVE
};

struct _par_proto_head {
  par_proto_op_t op;
  par_idx_t par;
  //par_len_t len;
};

#define PAR_PROTO_HEAD_LEN (sizeof(par_proto_head_t))

typedef uint8_t byte;

static byte _par_proto_crc8_update(byte crc, byte data){
  data ^= crc;
  crc = 0x00;
  if(data & 0x01) crc =0x5E;
  if(data & 0x02) crc^=0xBC;
  if(data & 0x04) crc^=0x61;
  if(data & 0x08) crc^=0xC2;
  if(data & 0x10) crc^=0x9D;
  if(data & 0x20) crc^=0x23;
  if(data & 0x40) crc^=0x46;
  if(data & 0x80) crc^=0x8C;
  return crc;
}

static byte _par_proto_crc8(const byte* data, byte len){
  byte crc;
  for(crc = 0x00; len--; crc = _par_proto_crc8_update(crc, *data++));
  return crc;
}

static byte _par_proto_unwrap(const byte *data, byte len){
  byte c = len - 1;
  return (_par_proto_crc8(data, c) == data[c]) ? c : 0;
}

static byte _par_proto_wrap(byte *data, byte len){
  data[len] = _par_proto_crc8(data, len);
  return len + 1;
}

/*
  Server
*/
#ifdef PAR_PROTO_SERVER

#include "par.h"

par_len_t par_proto_exec(par_raw_t *buf, par_len_t len){
  // parse request
  len = _par_proto_unwrap(buf, len);
  
  par_proto_head_t *head = buf;
  
  if(len >= PAR_PROTO_HEAD_LEN){
    if(head->op == PAR_PROTO_SETP){
      void *new_val = NULL;
      if(len - PAR_PROTO_HEAD_LEN == par_info_len(head->par)){ // set
        new_val = buf + PAR_PROTO_HEAD_LEN;
      }
#ifdef PAR_DEFAULT
      else if(len - PAR_PROTO_HEAD_LEN == 0){ // reset to default
        new_val = _par_def_h(par_info_ptr(head->par));
      }
#endif
      if(new_val != NULL){
        par_info_set(head->par, new_val);
      }
    }
#ifdef PAR_EEPROM
    if(head->op == PAR_PROTO_LOAD){
      par_load();
    }
    
    if(head->op == PAR_PROTO_SAVE){
      par_save();
    }
#endif
  }else{
    head->op = PAR_PROTO_NONE;
    head->par = PAR_PROTO_NO_PAR;
  }
  
  // produce response
  len = PAR_PROTO_HEAD_LEN; // use inlen as outlen
  
  if(head->op == PAR_PROTO_GETP){
    par_info_get(head->par, buf + PAR_PROTO_HEAD_LEN);
    len += par_info_len(head->par);
  }
  
  if(head->op == PAR_PROTO_INFO){
    if(head->par == PAR_PROTO_NO_PAR){
      *(byte*)(buf + len) = par_info_all();
      len += sizeof(byte);
    }else{
      const par_info_t *info = par_info_par(head->par);
      par_len_t slen;
#define _(field) {                              \
        slen = strlen(info->field) + 1;         \
        memcpy(buf + len, info->field, slen);   \
        len += slen;                            \
      }
      
      _(name); // Append name
      _(type); // Append type
      _(hint); // Append hint
      
#undef _
    }
  }
  
  return _par_proto_wrap(buf, len);
}

#endif//PAR_PROTO_SERVER

#ifdef PAR_PROTO_CLIENT

#include "par-proto-hif.h"
#include <stdlib.h>

static par_ret_t _par_proto_transfer_unsafe(par_dev_t *dev,
                                            par_proto_op_t op, par_idx_t par,
                                            const par_raw_t *in_data, par_len_t in_len,
                                            par_raw_t **out_data, par_len_t *out_len){
  
  if(!par_online(dev)){
    return PAR_DEV_CLOSED;
  }
  
  par_proto_head_t req = { op, par };
  
  dev->_buf[0] = 1;
  
  par_val_t *data = dev->_buf + 1;
  par_len_t len = 0;
  
  memcpy(data, &req, PAR_PROTO_HEAD_LEN);
  len += PAR_PROTO_HEAD_LEN;
  
  if(in_data != NULL && in_len > 0){
    memcpy(data + len, in_data, in_len);
    len += in_len;
  }
  
  len = _par_proto_wrap((byte*)data, len);
  
  if(!dev->_hif->send(dev->_dev, dev->_buf, len + 1)){
    return PAR_ERR_SEND;
  }
  
  len = PAR_PROTO_BUFFER_LEN - 1;
  
  if(!dev->_hif->recv(dev->_dev, dev->_buf, &len)){
    return PAR_ERR_RECV;
  }
  
  len = _par_proto_unwrap((byte*)data, len - 1);
  
  if(len < PAR_PROTO_HEAD_LEN){
    return PAR_ERR_CHECK;
  }
  
  par_proto_head_t res;
  
  memcpy(&res, data, PAR_PROTO_HEAD_LEN);
  
  if(res.op != req.op || res.par != req.par){
    return PAR_ERR_PROTO;
  }
  
  if(out_len > 0){
    *out_len = len - PAR_PROTO_HEAD_LEN;
    
    if(out_data != NULL){
      if(*out_len > 0){
        *out_data = data + PAR_PROTO_HEAD_LEN;
      }else{
        *out_data = NULL;
      }
    }
  }
  
  return PAR_OK;
}

#ifdef PAR_PROTO_RETRY
#  define _par_proto_transfer _par_proto_transfer_safe

#  ifdef PAR_PROTO_RETRY_DELAY
#    include <unistd.h>
#  endif

static par_ret_t _par_proto_transfer_safe(par_dev_t *dev,
                                          par_proto_op_t op, par_idx_t par,
                                          const par_raw_t *in_data, par_len_t in_len,
                                          par_raw_t **out_data, par_len_t *out_len){
  if(!par_online(dev)){
    return PAR_DEV_CLOSED;
  }
  
  par_len_t trynum = dev->_retry + 1;
  par_ret_t ret;
  
  for(; trynum > 0 && (ret = _par_proto_transfer_unsafe(dev, op, par, in_data, in_len, out_data, out_len)); trynum--){
#  ifdef PAR_PROTO_RETRY_DELAY
    usleep(PAR_PROTO_RETRY_DELAY);
#  endif
  }
  
  return ret;
}

#else//PAR_PROTO_RETRY
#  define _par_proto_transfer _par_proto_transfer_unsafe
#endif//PAR_PROTO_RETRY

par_ret_t par_test(par_dev_t *dev){
  par_ret_t ret = PAR_DEV_CLOSED;
  
  if(par_online(dev)){
    par_len_t len;
    par_raw_t *buf;
    
    ret = _par_proto_transfer(dev, PAR_PROTO_INFO, PAR_PROTO_NO_PAR,
                              NULL, 0, &buf, &len);
    
    if(ret == PAR_OK && len != 1){
      return PAR_ERR_PROTO;
    }
  }
  
  return ret;
}

static par_len_t par_info_req(par_dev_t *dev, par_idx_t par, par_inf_t *inf){
  par_len_t len;
  par_ret_t ret;
  par_raw_t *buf;
  
  if(inf == NULL){ // get num of params
    ret = _par_proto_transfer(dev, PAR_PROTO_INFO, PAR_PROTO_NO_PAR,
                              NULL, 0, &buf, &len);
    
    if(ret == PAR_OK && len == 1){
      return *(const par_len_t*)buf;
    }
    // potentially error
    return 0;
  }
  
  // get param info
  ret = _par_proto_transfer(dev, PAR_PROTO_INFO, par,
                            NULL, 0, &buf, &len);
  if(ret == PAR_OK && len > 0){
    const char *ptr = buf;
    
    inf->name = malloc(len);
    memcpy(inf->name, buf, len);
    
    inf->type = inf->name + strlen(inf->name) + 1;
    inf->hint = inf->type + strlen(inf->type) + 1;
    
    return 1;
  }
  // potentially error
  return 0;
}

static void par_info_del(par_dev_t *dev){
  if(dev->infos != NULL){
    for(par_len_t i = 0; i < dev->count; i++){
      if(dev->infos[i].name) free(dev->infos[i].name);
    }
    free(dev->infos);
    dev->infos = NULL;
    dev->count = 0;
  }
}

static par_ret_t par_info_new(par_dev_t *dev){
  dev->count = par_info_req(dev, PAR_PROTO_NO_PAR, NULL);
  
  if(dev->count > 0){
    par_len_t i;
    
    dev->infos = malloc(sizeof(par_inf_t) * dev->count);
    
    for(i = 0; i < dev->count; i++){
      memset(&dev->infos[i], 0, sizeof(par_inf_t));
    }
    
    for(i = 0; i < dev->count; i++){
      if(1 != par_info_req(dev, i, dev->infos + i)){
        par_info_del(dev);
        dev->count = 0;
        return PAR_ERR_NOPAR;
      }
    }
  }else{
    return PAR_ERR_NOPAR;
  }

  return PAR_OK;
}

par_len_t par_list(par_dev_t *dev, const par_inf_t **info){
  if(info){
    *info = dev->infos;
  }
  return dev->count;
}

const par_inf_t *par_info(par_dev_t *dev, par_idx_t par){
  return dev->count > par ? &dev->infos[par] : NULL;
}

par_ret_t par_load(par_dev_t *dev){
  return _par_proto_transfer(dev, PAR_PROTO_LOAD, -1,
                             NULL, 0, NULL, NULL);
}

par_ret_t par_save(par_dev_t *dev){
  return _par_proto_transfer(dev, PAR_PROTO_SAVE, -1,
                             NULL, 0, NULL, NULL);
}

par_idx_t par_find(par_dev_t *dev, const char *name){
  if(dev->count > 0){
    for(par_len_t i = 0; i < dev->count; i++){
      if(strcmp(par_info(dev, i)->name, name) == 0){
        return i;
      }
    }
  }
  return PAR_PROTO_NO_PAR;
}

par_ret_t par_get_raw(par_dev_t *dev, par_idx_t par, par_raw_t **val, par_len_t *len){
  if(par >= dev->count){
    return PAR_ERR_NOPAR;
  }
  
  par_ret_t ret = _par_proto_transfer(dev, PAR_PROTO_GETP, par,
                                      NULL, 0, val, len);
  if(ret != PAR_OK){
    return ret;
  }
  
  const char *type = par_info(dev, par)->type;
#define _(t, f) if(type && strcmp(type, #t) == 0){  \
    if(*len != sizeof(t)) return PAR_ERR_SIZE;      \
    type = NULL;                                    \
  }
  PAR_PROTO_TYPES;
#undef _
  
  if(type){
    return PAR_ERR_TYPE;
  }
  
  return PAR_OK;
}

par_ret_t par_set_raw(par_dev_t *dev, par_idx_t par, const par_raw_t *buf, par_len_t len){
  if(par >= dev->count){
    return PAR_ERR_NOPAR;
  }
  
  const char *type = par_info(dev, par)->type;

#define _(t, f) if(type && strcmp(type, #t) == 0){  \
    if(len < sizeof(t)) return PAR_ERR_SIZE;        \
    len = sizeof(t);                                \
    type = NULL;                                    \
  }
  PAR_PROTO_TYPES;
#undef _
  
  if(type){
    return PAR_ERR_TYPE;
  }
  
  return _par_proto_transfer(dev, PAR_PROTO_SETP, par,
                             buf, len, NULL, NULL);
}

#ifdef PAR_PROTO_STR
#include <stdio.h>
#include <string.h>

par_ret_t par_get_str(par_dev_t *dev, par_idx_t par, char **str){
  par_len_t len;
  par_raw_t *val;
  
  par_ret_t ret = par_get_raw(dev, par, &val, &len);
  
  if(ret != PAR_OK){
    return ret;
  }
  
  const char *type = par_info(dev, par)->type;
  
  static char buf[32];

  *str = buf;
  
#define _(t, f) if(strcmp(type, #t) == 0){            \
    snprintf(buf, sizeof(buf), "%" #f, *((t*)val));   \
    return PAR_OK;                                    \
  }
  PAR_PROTO_TYPES;
#undef _

  *str = NULL;

  return PAR_ERR_TYPE;
}

par_ret_t par_set_str(par_dev_t *dev, par_idx_t par, const char *str){
  if(par >= dev->count){
    return PAR_ERR_NOPAR;
  }
  
  par_len_t len = 0;
  par_val_t val[32];
  
  const char *type = par_info(dev, par)->type;
  
#define _(t, f) if(strcmp(type, #t)==0){        \
    sscanf(str, "%" #f, ((t*)val));             \
    len = sizeof(t);                            \
  }
  PAR_PROTO_TYPES;
#undef _
  
  if(len){
    return par_set_raw(dev, par, val, len);
  }
  
  return PAR_ERR_TYPE; // !unsupported type
}
#endif//PAR_PROTO_STR

par_ret_t par_def(par_dev_t *dev, par_idx_t par){
  if(par >= dev->count){
    return PAR_ERR_NOPAR;
  }
  return _par_proto_transfer(dev, PAR_PROTO_SETP, par,
                             NULL, 0, NULL, NULL);
}

#define _(type) _PAR_PROTO_HIF_EXTERN(type);
PAR_PROTO_HIFS
#undef _

static par_hif_t* _par_proto_hifs_[] = {
#define _(type) &_PAR_PROTO_HIF_SYMBOL(type, reg),
  PAR_PROTO_HIFS
#undef _
};

par_dev_t *par_new(){
  return malloc(sizeof(par_dev_t));
}

void par_del(par_dev_t *dev){
  free(dev);
}

par_ret_t par_open(par_dev_t *dev, const char *uri){
  if(par_online(dev)){
    return PAR_DEV_OPENED;
  }
  
  dev->_hif = NULL;
  
  for(par_len_t i = 0; i < sizeof(_par_proto_hifs_)/sizeof(par_hif_t*); i++){
    par_len_t tl = strlen(_par_proto_hifs_[i]->type);
    if(0 == strncmp(uri, _par_proto_hifs_[i]->type, tl)){
      dev->_hif = _par_proto_hifs_[i];
      uri += tl;
      break;
    }
  }
  
#ifdef PAR_PROTO_RETRY
  if(uri[0] == '#'){
    uri++;
    char *end = NULL;
    dev->_retry = strtol(uri, &end, 10);
    if(end && end > uri){
      uri = end;
    }
  }else{
    dev->_retry = PAR_PROTO_RETRY;
  }
#endif
  
  if(!dev->_hif){
    return PAR_ERR_NOHIF;
  }
  
  dev->_hif->new(&dev->_dev);
  
  if(!dev->_hif->open(dev->_dev, uri)){
    return PAR_ERR_OPEN;
  }
  
  return par_info_new(dev);
}

par_ret_t par_close(par_dev_t *dev){
  if(!par_online(dev)){
    return PAR_DEV_CLOSED;
  }
  
  par_info_del(dev);
  
  dev->_hif->clos(dev->_dev);
  dev->_hif->del(&dev->_dev);
  
  dev->_hif = NULL;
}

const char* par_ret_str(par_dev_t *dev, par_ret_t ret){
  switch(ret){
  case PAR_OK: return "OK";
    
  case PAR_ERR_OPEN: return "Error opening device";
  case PAR_ERR_SEND: return "Error sending to device";
  case PAR_ERR_RECV: return "Error receiving from device";
  case PAR_ERR_CHECK: return "Error data transfer with device";
  case PAR_ERR_PROTO: return "Error operation with device";
    
  case PAR_ERR_NOPAR: return "Parameter(s) not found";
  case PAR_ERR_NOHIF: return "Host Interface(s) not found";

  case PAR_DEV_OPENED: return "Device already opened";
  case PAR_DEV_CLOSED: return "Device closed";
  }
  return NULL;
}

#endif//PAR_PROTO_CLIENT
