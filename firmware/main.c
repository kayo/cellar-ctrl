#include<avr/io.h>
#include<util/delay.h>
#include<avr/interrupt.h>
#include<avr/eeprom.h>
#include<avr/sleep.h>
#include<avr/wdt.h>
#include<stdlib.h>
#include<string.h>

#include"7seg.h"
#include"btn.h"
#include"menu.h"

#include"dht11.h"
#include"ctl.h"

#include"par.h"
#include"par-vusb.h"
#include"par-rf24.h"

#define no_value 255
static inline void print_value(const char* lab, uint8_t len, uint8_t val){
  static char buf[5];
  sevseg_print(lab);
  if(val != no_value){
    sevseg_window(0, len);
#ifdef USE_SPRINTF
    sprintf(buf, "%2d", val);
#else
    buf[0] = ' ';
    itoa(val, buf + (val < 10 ? 1 : 0), 10);
#endif
    sevseg_print(buf);
    sevseg_window(0, 4);
  }
}
static inline void print_temp(uint8_t val){
  print_value("--*C", 2, val);
}
static inline void print_humi(uint8_t val){
  print_value("--*%", 2, val);
}

static inline void print_state(const char* lab, uint8_t st){
  sevseg_print(lab);
  if(st){
    static unsigned char ON[] = {
      SEVSEG_A | SEVSEG_B | SEVSEG_C | SEVSEG_D | SEVSEG_E | SEVSEG_F, // O
      SEVSEG_A | SEVSEG_B | SEVSEG_C | SEVSEG_E | SEVSEG_F, // N
    };
    sevseg_window(2, 2);
    sevseg_output(ON);
    sevseg_window(0, 4);
  }
}
static inline void print_heater(uint8_t st){
  print_state("H---", st);
}
static inline void print_cooler(uint8_t st){
  print_state("C---", st);
}

static inline void print_mode(uint8_t mode){
  sevseg_print(mode ? "AAAA" : "HHHH");
}

MENU_EVT(tick){
  MENU_ITEM(default){
    MENU_TICK(0){ print_mode(par_get(auto_mode)); return; }
    MENU_TICK(1){ print_temp(par_get(temp_curr)); return; }
    MENU_TICK(2){ print_humi(par_get(humi_curr)); return; }
  }
  MENU_ITEM(temp_edge){
    MENU_TICK(0){ print_temp(par_get(temp_edge)); return; }
    MENU_TICK(1){ print_temp(no_value); return; }
  }
  MENU_ITEM(humi_edge){
    MENU_TICK(0){ print_humi(par_get(humi_edge)); return; }
    MENU_TICK(1){ print_humi(no_value); return; }
  }
  MENU_ITEM(heater_on){ print_heater(par_get(auto_mode) ? par_get(heater_en) : par_get(heater_on)); return; }
  MENU_ITEM(cooler_on){ print_cooler(par_get(auto_mode) ? par_get(cooler_en) : par_get(cooler_on)); return; }
}

MENU_EVT(oper){
  MENU_ITEM(default){
    MENU_OPER(minus){ par_set(auto_mode, 0); return; }
    MENU_OPER(plus) { par_set(auto_mode, 1); return; }
  }
  MENU_ITEM(temp_edge){
    MENU_OPER(minus){
      if(par_get(temp_edge) > 0){
        par_set(temp_edge, par_get(temp_edge) - 1);
      }
      return;
    }
    MENU_OPER(plus){
      if(par_get(temp_edge) < 30){
        par_set(temp_edge, par_get(temp_edge) + 1);
      }
      return;
    }
  }
  MENU_ITEM(humi_edge){
    MENU_OPER(minus){
      if(par_get(humi_edge) > 0){
        par_set(humi_edge, par_get(humi_edge) - 1);
      }
      return;
    }
    MENU_OPER(plus){
      if(par_get(humi_edge) < 100){
        par_set(humi_edge, par_get(humi_edge) + 1);
      }
      return;
    }
  }
  MENU_ITEM(heater_on){
    MENU_OPER(minus){ if(par_get(auto_mode)) par_set(heater_en, 0); else par_set(heater_on, 0); }
    MENU_OPER(plus) { if(par_get(auto_mode)) par_set(heater_en, 1); else par_set(heater_on, 1); }
    return;
  }
  MENU_ITEM(cooler_on){
    MENU_OPER(minus){ if(par_get(auto_mode)) par_set(cooler_en, 0); else par_set(cooler_on, 0); }
    MENU_OPER(plus) { if(par_get(auto_mode)) par_set(cooler_en, 1); else par_set(cooler_on, 1); }
    return;
  }
}

BTN_EVT(mode){ if(state) menu_next(); }
BTN_EVT(minus){ if(state) menu_oper(minus); }
BTN_EVT(plus){ if(state) menu_oper(plus); }

PAR_EVT(auto_mode){ par_save(); }
PAR_EVT(temp_edge){ par_save(); }
PAR_EVT(humi_edge){ par_save(); }

PAR_EVT(heater_on){ ctl_state(heater_on, val); }
PAR_EVT(cooler_on){ ctl_state(cooler_on, val); }

static inline void led_update(){
  static uint8_t i = 0;
  if(i == 0){
    ctl_state(heater_led, par_get(heater_on));
  }else if(i == 1){
    ctl_state(heater_led, 0);
    ctl_state(cooler_led, par_get(cooler_on));
  }else if(i == 2){
    ctl_state(cooler_led, 0);
  }
  i++;
  if(i > 4) i = 0;
}

static inline void auto_tick(){
  if(par_get(auto_mode)){
    if(par_get(temp_curr) == no_value){
      par_set(heater_on, 0);
    }else{
      if(par_get(heater_en) && par_get(temp_curr) < par_get(temp_edge)){
        par_set(heater_on, 1);
      }else{
        par_set(heater_on, 0);
      }
    }
    if(par_get(humi_curr) == no_value){
      par_set(cooler_on, 0);
    }else{
      if(par_get(cooler_en) && par_get(humi_curr) > par_get(humi_edge)){
        par_set(cooler_on, 1);
      }else{
        par_set(cooler_on, 0);
      }
    }
  }
}

static inline void tui_init(){
  TCCR0 = _BV(WGM01)         /* CTC mode of operation (Clear Timer on Compare match) */
    | _BV(CS02) | _BV(CS00); /* Clock Select through prescaler CLK/1024 */
  TCNT0 = 0;                 /* Clear Timer Counter */
  OCR0 = 42; //77;  // 100Hz      /* Compare value (Clear timer counter after 155 ticks) */
  //TIMSK |= _BV(OCIE0);       /* Enable Compare Match Interrupt  */
}

__attribute__((__noreturn__)) void main(){
  wdt_disable();
  
  /* initialize libraries */
  sevseg_init();
  ctl_init();
  btn_init();
  tui_init();
  
  par_vusb_init();
  par_rf24_init();
  
  /* setup button press events */
  btn_evt(mode, 1);
  btn_evt(minus, 1);
  btn_evt(plus, 1);
  
  /* setup parameter change events */
  par_evt(auto_mode, 1);
  par_evt(temp_edge, 1);
  par_evt(humi_edge, 1);
  
  par_evt(heater_on, 1);
  par_evt(cooler_on, 1);
  
  /* Load parameter's values */
  par_load();
  
  /* enable ui */
  btn_enable();
  sevseg_enable();
  
  sei();
  
  dht11_select_port(0);
  sevseg_print("8.8.8.8.");
  
  par_set(temp_curr, no_value);
  par_set(humi_curr, no_value);
  
  for(;;){
    par_vusb_poll();
    par_rf24_poll();
    if(TIFR & _BV(OCF0)){ // F ~ 200 Hz
      static uint8_t tick = 0;
      static uint8_t meas = 0;
      if(tick >= 255){ // T ~ 0.5 sec
        menu_tick();
        auto_tick();
        tick = 0;
        if(meas == 12){
          dht11_select_port(0);
          meas = 0;
        }else if(meas == 6){ // T ~ 3 sec
          dht11_data_t data;
          uint8_t ret;
          cli();
          dht11_prepare();
          ret = dht11_read_data(&data);
          sei();
          par_set(error, ret);
          if(ret == DHT11_SUCCESS){ // data received
            par_set(temp_curr, data.t.i);
            par_set(humi_curr, data.h.i);
            meas = 0;
          }else{ // error on receiving data
            dht11_select_port(DHT11_NO_PORT); // power off port
            par_set(temp_curr, no_value);
            par_set(humi_curr, no_value);
          }
        }else{
          meas ++;
        }
      }else{
        tick ++;
      }
      
      sevseg_update();
      led_update();
      btn_update();
      TIFR |= _BV(OCF0);
    }
  }
}
