# 7-segment display
ifeq ($(sevseg),on)
  def+=SEVSEG=1
  ifneq ($(findstring progmem,$(sevseg.features)),)
    def+=SEVSEG_PROGMEM=1
  endif
  def+=SEVSEG_TYPE=SEVSEG_COMMON_$(if $(findstring anode,$(sevseg.features)),ANODE,CATODE)
  def+=SEVSEG_SEGMENTS='$(call parsespec,$(sevseg.segments))'
  def+=SEVSEG_DIGITS='$(call parsespec,$(sevseg.digits))'
endif
