#include"btn.h"
#ifdef BTN
#include<avr/io.h>
#include<stdlib.h>

btn_ios_t _btn_ios = {
#define _(n, p, w) {0, 0, NULL},
  BTN
#undef _
};

void btn_enable(){
#define _(n, p, w) (PORT ## p) |= _BV(w);
  BTN
#undef _
}

void btn_disable(){
#define _(n, p, w) (PORT ## p) &= ~_BV(w);
  BTN
#undef _
}

#define btn_sa(v, c) (v) += (c) ? ((v) > 0 ? -1 : 0) : ((v) < (BTN_EDGE) ? 1 :0)
#define btn_st(n) ({						\
      if(_btn_ios.n.val == 0 && _btn_ios.n.state == 1){		\
	_btn_ios.n.state = 0;					\
	if(_btn_ios.n.event) _btn_ios.n.event(0);		\
      }								\
      if(_btn_ios.n.val == BTN_EDGE && _btn_ios.n.state == 0){	\
	_btn_ios.n.state = 1;					\
	if(_btn_ios.n.event) _btn_ios.n.event(1);		\
      }								\
    })
void btn_update(){
#define _(n, p, w) btn_sa(_btn_ios.n.val, PIN ## p & _BV(w)); btn_st(n);
  BTN
#undef _
}

#endif
