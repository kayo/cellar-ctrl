hexes2bytes=$(shell echo '$(1)' | sed -r -e 's/([0-9a-f]{2})/0x\1,/g' -e 's/,[^,]*$$//')

ifeq ($(par),on)
  par.rf24.addr.bytes:=$(call hexes2bytes,$(par.rf24.addr))
  def+=PAR_RF24_ADDR=$(par.rf24.addr.bytes)
  ifeq ($(par.rf24.cypher),aes)
    def+=PAR_RF24_AES=1
    ifneq ($(par.rf24.passwd),)
      par.rf24.aes.key:=$(shell printf "%s" '$(par.rf24.passwd)' | sha256sum)
    endif
    par.rf24.aes.key.bytes:=$(call hexes2bytes,$(par.rf24.aes.key))
    def+=PAR_RF24_AES_KEY='$(par.rf24.aes.key.bytes)'
    def+=PAR_RF24_AES_BITS=$(call calc,$(popnchar)0$(patsubst %,+1,$(subst $(comachar), ,$(par.rf24.aes.key.bytes)))$(pclschar)*8)
  endif
endif
