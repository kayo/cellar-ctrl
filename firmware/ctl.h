#ifndef __CTL_H__
#define __CTL_H__ "ctl.h"
#ifdef CTL

#include <stdint.h>

typedef struct _ctl_io ctl_io_t;
struct _ctl_io {
  volatile uint8_t *port;
  uint8_t mask;
};

typedef struct _ctl_ios ctl_ios_t;
struct _ctl_ios {
#define _(n, p, w) ctl_io_t n;
  CTL
#undef _
};

extern ctl_ios_t _ctl_ios;

#define ctl_ptr(i) (((ctl_io_t*)&_ctl_ios)[i])
void ctl_init();

#define ctl_state(n, s) ((s) ? (*(_ctl_ios.n.port) |= _ctl_ios.n.mask) : (*(_ctl_ios.n.port) &= ~_ctl_ios.n.mask))

#endif
#endif
