#include "delay.h"

#ifdef __unix__

#include <time.h>

void _delay_us(double __us){
  struct timespec req;
	req.tv_sec = 0;
	req.tv_nsec = __us * 1000L;
	nanosleep(&req, NULL);
}

#endif
