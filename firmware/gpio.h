#ifndef __GPIO_H__
#define __GPIO_H__

#include <stdint.h>

typedef /*enum _gpio_ret*/ uint8_t gpio_ret_t;
typedef uint8_t gpio_pin_t;
typedef uint8_t gpio_val_t;
typedef struct _gpio_port_ gpio_port_t;
typedef struct _gpio_dev gpio_dev_t;

//void gpio_init(gpio_dev_t* dev);
gpio_ret_t gpio_open(gpio_dev_t* dev, gpio_pin_t pin);
gpio_ret_t gpio_close(gpio_dev_t* dev);

void gpio_set_dir(gpio_dev_t* dev, gpio_val_t dir);
gpio_val_t gpio_get_dir(gpio_dev_t* dev);

void gpio_set_val(gpio_dev_t* dev, gpio_val_t val);
gpio_val_t gpio_get_val(gpio_dev_t* dev);

enum _gpio_ret{
  GPIO_NO_ERROR = 0,
  GPIO_CANT_SETUP_PIN,
#ifdef __unix__
  GPIO_CANT_OPEN_DIR,
  GPIO_CANT_OPEN_VAL,
#endif//__unix__
  GPIO_NOT_OPENED
};

struct _gpio_dev{
  gpio_pin_t _pin;
#ifdef __unix__
  int _dir_fd;
  int _val_fd;
#else
  const gpio_port_t *_port;
#endif
};

#ifdef GPIO_ERROR_STRING
const char* gpio_error(gpio_ret_t ret);
#endif

#endif//__GPIO_H__
