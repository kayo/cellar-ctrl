#include"par.h"

typedef struct{
#define _(name, type, hint, defv) type name;
  PAR
#undef _
} _par_val_t;
static _par_val_t _par_val = {
#define _(name, type, hint, defv) defv,
  PAR
#undef _
};

const _par_ptr_t _par_ptr = {
#define _(name, type, hint, defv) &(_par_val.name),
  PAR
#undef _
  ((void*)&_par_val) + sizeof(_par_val_t)
};

#ifdef PAR_EVENT
_par_evt_t _par_evt = {
#define _(name, type, hint, defv) 0,
  PAR
#undef _
  0
};
#endif

#ifdef PAR_DEFAULT
const _par_val_t _par_def = {
#define _(name, type, hint, defv) defv,
  PAR
#undef _
};
void *_par_def_h(void *var_ptr){
  return (var_ptr - (void*)&_par_val + (void*)&_par_def);
}
#endif

#ifdef PAR_INFO
const par_info_t _par_info[] = {
#define _(name, type, hint, defv) { #name, #type, #hint },
  PAR
#undef _
  { 0, 0 }
};
#endif

#ifdef PAR_EEPROM
#include<avr/eeprom.h>
static EEMEM uint8_t _par_val_empty = 1;
static EEMEM _par_val_t _par_val_eeprom = {
#define _(name, type, hint, defv) defv,
  PAR
#undef _
};
void par_load(){
  eeprom_busy_wait();
  if(eeprom_read_byte(&_par_val_empty) == 0){ // eeprom ok
    eeprom_busy_wait();
    eeprom_read_block(&_par_val, &_par_val_eeprom, sizeof(_par_val_t));
  }
}
void par_save(){
  eeprom_busy_wait();
  eeprom_update_block(&_par_val, &_par_val_eeprom, sizeof(_par_val_t));
  eeprom_busy_wait();
  eeprom_write_byte(&_par_val_empty, 0);
}
#endif
