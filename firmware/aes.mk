ifeq ($(aes),on)
  aes.path=aes

  #aes.key?=asm
  #aes.enc?=asm
  #aes.dec?=asm-fast

  ifeq ($(aes.key),c)
    aes.obj+=aes_keyschedule.o
  endif
  ifeq ($(aes.key),asm)
    aes.obj+=aes_keyschedule-asm.o
  endif

  ifeq ($(aes.enc),c)
    aes.obj+=$(patsubst %,aes%_enc.o,128 192 256) aes_sbox.o aes_enc.o gf256mul.o
  endif
  ifeq ($(aes.enc),asm)
    aes.obj+=aes_sbox-asm.o aes_enc-asm.o
  endif

  ifeq ($(aes.dec),c)
    aes.obj+=$(patsubst %,aes%_dec.o,128 192 256) aes_invsbox.o aes_dec.o gf256mul.o
  endif
  ifeq ($(aes.dec),asm)
    aes.obj+=aes_invsbox-asm.o aes_dec-asm.o
  endif
  ifeq ($(aes.dec),asm-fast)
    aes.obj+=aes_invsbox-asm.o aes_dec-asm_faster.o
  endif

  cc.dir+=$(aes.path)
  obj+=$(addprefix $(aes.path)/,$(aes.obj))
endif
