#ifdef DHT11

#include"dht11.h"
#include<util/delay.h>
#include<avr/io.h>
#include<stdlib.h>

typedef struct _dht11_io_ dht11_io_t;
struct _dht11_io_ {
  volatile uint8_t *ddr;
  volatile uint8_t *port;
  volatile uint8_t *pin;
  uint8_t mask;
};

static dht11_io_t dht11_ports[] = {
#define _(p, w) { &(DDR ## p), &(PORT ## p), &(PIN ## p), _BV(w) },
  DHT11
#undef _
};
#define DHT11_NUM_PORTS() (sizeof(dht11_ports)/sizeof(dht11_io_t))

static dht11_io_t dht11_port = { NULL, NULL, NULL, 0 };

static inline void dht11_input(){
  *(dht11_port.ddr) &= ~dht11_port.mask;
}
static inline void dht11_output(){
  *(dht11_port.ddr) |= dht11_port.mask;
}

static inline void dht11_pulldown(){
  *(dht11_port.port) &= ~dht11_port.mask;
}
static inline void dht11_pullup(){
  *(dht11_port.port) |= dht11_port.mask;
}

#define dht11_read_bit() (*(dht11_port.pin) & dht11_port.mask)

#ifdef DHT11_SAFE
#define DHT_SAFE_EXIT_IF_NO_PORT(n) if(dht11_port.mask == 0) return (n);
#else
#define DHT_SAFE_EXIT_IF_NO_PORT(n)
#endif

void dht11_select_port(uint8_t port){
  if(port == DHT11_NO_PORT){
    //dht11_input();
    dht11_pulldown();
    return;
  }
  
#ifdef DHT11_SAFE
  if(port > DHT11_NUM_PORTS()){
    return;
  }
#endif
  dht11_port.ddr = dht11_ports[port].ddr;
  dht11_port.port = dht11_ports[port].port;
  dht11_port.pin = dht11_ports[port].pin;
  dht11_port.mask = dht11_ports[port].mask;
  
  dht11_output();
  dht11_pullup();
}

uint8_t dht11_num_ports(){
  return DHT11_NUM_PORTS();
}

uint8_t dht11_read_byte(){
  uint8_t i = 0;
  uint8_t d = 0;
  
  DHT_SAFE_EXIT_IF_NO_PORT(0);
  
  for(i = 0; i < 8; i++){ // read bit to bit
    for(; !dht11_read_bit(); ); // wait for 50us
    _delay_us(30);
    if(dht11_read_bit()){
      d |= _BV(7-i);
    }
    for(; dht11_read_bit(); );  // wait '1' finish
  }
  return d;
}

void dht11_prepare(){ // pull-down i/o pin for 18ms
  dht11_pulldown();
  _delay_ms(18);
}

uint8_t dht11_read_data(dht11_data_t *data){
  uint8_t cs;
  uint8_t i;
  
  DHT_SAFE_EXIT_IF_NO_PORT(DHT11_ERROR_NULL_PORT);
  
  dht11_pullup();
  _delay_us(40);
  dht11_input();
  _delay_us(40);
  
  if(dht11_read_bit()){
    return DHT11_CONDIDION_1_NOT_MET;
  }
  
  _delay_us(80);
  if(!dht11_read_bit()){
    return DHT11_CONDITION_2_NOT_MET;
  }
  
  _delay_us(80); // now ready for data reception
  for(i = 0; i < sizeof(dht11_data_t); i++){
    data->v[i] = dht11_read_byte();
  }
  
  dht11_output();
  //dht11_pullup(); // not needed?
  
  for(cs = 0, i = 0; i < sizeof(dht11_data_t) - 1; i++){ // calc check sum
    cs += data->v[i];
  }
  
  if(cs != data->cs){
    return DHT11_ERROR_CHECK_SUM;
  }
  
  return DHT11_SUCCESS;
}

#endif
