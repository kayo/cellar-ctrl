#ifndef __DELAY_H__
#define __DELAY_H__

#ifdef __unix__
#  define _delay_ms(__ms) _delay_us(__ms * 1000);
void _delay_us(double __us);
#endif

#ifdef __AVR__
#  include <util/delay.h>
#endif

#endif//__DELAY_H__
