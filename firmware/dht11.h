#ifndef __dht11_h__
#define __dht11_h__ "dht11.h"

#ifdef DHT11

#define DHT11_NO_PORT 255

#define DHT11_SUCCESS 0x00
#define DHT11_CONDIDION_1_NOT_MET 0x11
#define DHT11_CONDITION_2_NOT_MET 0x12
#define DHT11_ERROR_NULL_PORT 0x23
#define DHT11_ERROR_CHECK_SUM 0x31

#include <stdint.h>

typedef struct _dht11_val_ dht11_val_t;
struct _dht11_val_ {
  uint8_t i; // integral part
  uint8_t d; // decimal part
};

typedef union _dht11_data_ dht11_data_t;
union _dht11_data_ {
  struct {
    dht11_val_t h;  /* humidity */
    dht11_val_t t;  /* temperature */
    uint8_t cs;     /* check sum */
  };
  uint8_t v[5];
};

uint8_t dht11_read_byte();
uint8_t dht11_read_data(dht11_data_t *data);
void dht11_prepare();
uint8_t dht11_num_ports();
void dht11_select_port(uint8_t port);

#endif
#endif
