#if defined(MENU_ITEMS)
#ifndef __MENU_H__
#define __MENU_H__ "menu.h"

#include <stdint.h>

#ifdef MENU_ITEMS
enum {
  __menu_item_beg_,
#define _(name) __menu_item_ ## name ## _item_,
  MENU_ITEMS
#undef _
  __menu_item_end_
};
#endif

#ifdef MENU_AUTO_RESET
extern uint8_t __menu_auto_reset_tick_;
#define MENU_TOUCH() __menu_auto_reset_tick_ = 0;
#else
#define MENU_TOUCH()
#endif

#define MENU_EVT(event) void __menu_evt_ ## event ## _handler_()
#define MENU_FIRE(event) __menu_evt_ ## event ## _handler_();
#define MENU_ITEM(item) if(__menu_item_curr_ == __menu_item_ ## item ## _item_)

#ifdef MENU_TICKS
#define MENU_TICK(tick) if(__menu_tick_curr_ == tick && ++__menu_tick_curr_)
#define MENU_TICK_FIRE() MENU_FIRE(tick)
extern MENU_EVT(tick);
extern uint8_t __menu_tick_curr_;
#define MENU_TICK_RESET() __menu_tick_curr_ = 0;
#define MENU_TICK_ASYNC()				\
  MENU_TICK_RESET();    /* reset current tick */	\
  MENU_TICK_FIRE();     /* call tick handler */		\
  //MENU_TICK_RESET();
void menu_tick();
#else
#define MENU_TICK_FIRE()
#define MENU_TICK_RESET()
#define MENU_TICK_ASYNC()
#endif

#ifdef MENU_OPS
extern MENU_EVT(oper);
typedef struct _menu_oper_ menu_oper_t;
struct _menu_oper_ {
#define _(name) uint8_t name:1;
  MENU_OPS
#undef _
};
extern menu_oper_t __menu_oper_curr_;
#define MENU_OPER(name) if(__menu_oper_curr_.name)
#define menu_oper(name) ({			\
      __menu_oper_curr_.name = 1;		\
      MENU_FIRE(oper);				\
      MENU_TOUCH();				\
      MENU_TICK_ASYNC();			\
      __menu_oper_curr_.name = 0;		\
    })
#endif

extern uint8_t __menu_item_curr_;

void menu_goto(uint8_t item);
void menu_prev();
void menu_next();
void menu_reset();

#endif
#endif
