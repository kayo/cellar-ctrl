#ifndef __BTN_H__
#define __BTN_H__ "btn.h"
#ifdef BTN

#include <stdint.h>

typedef void(*btn_evt_f)(uint8_t state);
typedef struct _btn_io btn_io_t;
struct _btn_io {
  uint8_t val;
  uint8_t state;
  btn_evt_f event;
};

typedef struct _btn_ios btn_ios_t;
struct _btn_ios {
#define _(n, p, w) btn_io_t n;
  BTN
#undef _
};

extern btn_ios_t _btn_ios;

#define btn_init()
void btn_enable();
void btn_disable();
void btn_update();

#define btn_state(n) (_btn_ios.n.state)

#define BTN_EVT(n) void _btn_ ## n ## _evt_ (uint8_t state)
#define btn_evt(n, s) (_btn_ios.n.event = (s) ? _btn_ ## n ## _evt_ : 0)

#endif
#endif
