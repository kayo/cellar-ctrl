#ifdef __AVR__
#  include <avr/pgmspace.h>
#else
#  define PROGMEM
#  define pgm_read_byte(ptr) (*(ptr))
#endif
