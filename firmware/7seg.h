#ifndef __7SEG_H__
#define __7SEG_H__ "7seg.h"

#define SEVSEG_COMMON_CATODE 1
#define SEVSEG_COMMON_ANODE 2

#define SEVSEG_BV(n) (1<<(n))

#define SEVSEG_p 0
#define SEVSEG_a 1
#define SEVSEG_b 2
#define SEVSEG_c 3
#define SEVSEG_d 4
#define SEVSEG_e 5
#define SEVSEG_f 6
#define SEVSEG_g 7

#define SEVSEG_N 0

#define SEVSEG_P SEVSEG_BV(SEVSEG_p)
#define SEVSEG_A SEVSEG_BV(SEVSEG_a)
#define SEVSEG_B SEVSEG_BV(SEVSEG_b)
#define SEVSEG_C SEVSEG_BV(SEVSEG_c)
#define SEVSEG_D SEVSEG_BV(SEVSEG_d)
#define SEVSEG_E SEVSEG_BV(SEVSEG_e)
#define SEVSEG_F SEVSEG_BV(SEVSEG_f)
#define SEVSEG_G SEVSEG_BV(SEVSEG_g)

#define sevseg_init()
void sevseg_update();
void sevseg_enable();
void sevseg_disable();
void sevseg_window(unsigned char beg, unsigned char len);
void sevseg_print_(const unsigned char *buf);
#define sevseg_print(buf) sevseg_print_((const unsigned char *)(buf))
void sevseg_output(const unsigned char *buf);
#ifdef SEVSEG_PRINTF
void sevseg_printf(const char *buf, ...);
#endif

#endif
