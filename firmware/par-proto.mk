par.types?=int8_t:d int16_t:d int32_t:d uint8_t:u uint16_t:u uint32_t:u

ifneq ($(par.types),)
  def+=PAR_PROTO_TYPES='$(call parsespec,$(par.types))'
endif

ifneq ($(filter str,$(par.features)),)
  def+=PAR_PROTO_STR=1
endif

ifneq ($(par.retry),)
  def+=PAR_PROTO_RETRY=$(par.retry)
  ifneq ($(par.retry.delay),)
    def+=PAR_PROTO_RETRY_DELAY=$(par.retry.delay)
  endif
endif

par.hifs?=usb rf24
ifneq ($(par.hifs),)
  def+=PAR_PROTO_HIFS='$(call parsespec,$(par.hifs))'
  def+=$(patsubst %,PAR_PROTO_HIF_%=1,$(par.hifs))
endif
