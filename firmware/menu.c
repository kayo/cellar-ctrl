#if defined(MENU_ITEMS)
#include"menu.h"

uint8_t __menu_item_curr_ = 0;


menu_oper_t __menu_oper_curr_ = {
#define _(name) 0,
  MENU_OPS
#undef _
};

#ifdef MENU_AUTO_RESET
uint8_t __menu_auto_reset_tick_ = 0;
#endif

#ifdef MENU_TICKS
uint8_t __menu_tick_curr_ = 0;

void menu_tick(){
  uint8_t tick = __menu_tick_curr_;
  MENU_FIRE(tick);
  if(tick != 0 && tick == __menu_tick_curr_){ // if tick not equal null and not incremented, it's out of range, reset tick's counter and recall handler
    __menu_tick_curr_ = 0;
     MENU_FIRE(tick);
  }
#ifdef MENU_AUTO_RESET
  if(__menu_item_curr_ != __menu_item_beg_ + 1){ // nothing to be do on default item
    if(__menu_auto_reset_tick_ >= MENU_AUTO_RESET){
      menu_reset(); // reset if limit was been excessed
    }
    __menu_auto_reset_tick_ ++; // increment on each tick
  }
#endif
}
#endif

void menu_goto(uint8_t item){
#ifdef MENU_CYCLIC // ciclic navigation
  if(item <= __menu_item_beg_){
    item = __menu_item_end_ - 1;
  }
  if(item >= __menu_item_end_){
    item = __menu_item_beg_ + 1;
  }
#else
  if(item <= __menu_item_beg_){
    item = __menu_item_beg_ + 1;
  }
  if(item >= __menu_item_end_){
    item = __menu_item_end_ - 1;
  }
#endif
  __menu_item_curr_ = item; // set current item

#ifdef MENU_TICKS
  __menu_tick_curr_ = 0;    // reset current tick
  MENU_TICK_FIRE();         // call tick handler
  __menu_tick_curr_ = 0;
#endif

  MENU_TOUCH();             // reset on activity
}
void menu_prev(){
  menu_goto(__menu_item_curr_-1);
}
void menu_next(){
  menu_goto(__menu_item_curr_+1);
}
void menu_reset(){
  menu_goto(__menu_item_beg_+1);
}

#endif
