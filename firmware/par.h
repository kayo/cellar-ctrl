#ifndef PAR_H
#define PAR_H "par.h"

#include <stdint.h>
#include <stdbool.h>

#define par_type(name) _par_type_ ## name
#define _(name, type, hint, defv) typedef type par_type(name);
PAR
#undef _

typedef struct{
#define _(name, type, hint, defv) void *name;
  PAR
#undef _
  void *__end__;
} _par_ptr_t;
extern const _par_ptr_t _par_ptr;

#define par_ptr(name) _par_ptr.name
#define par_get(name) (*((par_type(name)*)par_ptr(name)))

#ifdef PAR_EVENT
typedef void(*_par_evt_f)(void *val);
typedef struct{
#  define _(name, type, hint, defv) _par_evt_f name;
  PAR
#  undef _
  _par_evt_f __end__;
} _par_evt_t;
extern _par_evt_t _par_evt;

#  define PAR_EVT(name) void _par_evt_ ## name ## _ (par_type(name) val)
#  define par_evt(name, stat)                                         \
  (_par_evt.name = (stat) ? (_par_evt_f)_par_evt_ ## name ## _ : 0)

#  define par_set(name, val) ({ par_get(name) = val; if(_par_evt.name) ((void(*)(par_type(name)))_par_evt.name)(val); })
#else
#  define par_set(name, val) (par_get(name) = val);
#endif

#ifdef PAR_DEFAULT
void *_par_def_h(void *var_ptr);
#  define par_def(name) (*((par_type(name)*)_par_def_h(par_ptr(name))))
#else
#  define par_def(name)
#endif

#ifdef PAR_INFO
typedef struct{
  const char *name;
  const char *type;
  const char *hint;
} par_info_t;
extern const par_info_t _par_info[];
#  define par_info_ptr(par) (((void**)&_par_ptr)[par])
#  define par_info_len(par) ((unsigned char)(par_info_ptr(par+1)-par_info_ptr(par)))
#  define par_info_all() ((sizeof(_par_ptr_t)-1)/sizeof(void*)) //({ unsigned char i; for(i = 0; _par_info[i].name; i++); i; })
#  define par_info_par(par) &(_par_info[par])
#  define par_info_get(par, ptr) memcpy(ptr, par_info_ptr(par), par_info_len(par));
#  ifdef PAR_EVENT
#    define par_info_evt(par) (((_par_evt_f*)&_par_evt)[par])
#    define par_info_set(par, ptr) ({                         \
      if(par_info_evt(par)) par_info_evt(par)(ptr);           \
      memcpy(par_info_ptr(par), (ptr), par_info_len(par));		\
    })
#  else
#    define par_info_set(par, ptr) ({                     \
      memcpy(par_info_ptr(par), ptr, par_info_len(par));	\
    })
#  endif
#endif

#ifdef PAR_EEPROM
void par_load();
void par_save();
#else
#  define par_load()
#  define par_save()
#endif

#endif
