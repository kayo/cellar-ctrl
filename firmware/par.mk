# parameters
ifeq ($(par),on)
  def+=PAR='$(call parsespec,$(par.elements))'
  ifneq ($(findstring eeprom,$(par.features)),)
    def+=PAR_EEPROM=1
  endif
  ifneq ($(findstring default,$(par.features)),)
    def+=PAR_DEFAULT=1
  endif
  ifneq ($(findstring event,$(par.features)),)
    def+=PAR_EVENT=1
  endif
  ifneq ($(findstring info,$(par.features)),)
    def+=PAR_INFO=1
  endif
endif
