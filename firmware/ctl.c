#include"ctl.h"
#ifdef CTL
#include<avr/io.h>
#include<stdlib.h>

ctl_ios_t _ctl_ios = {
#define _(n, p, w) {&(PORT ## p), _BV(w)},
  CTL
#undef _
};

void ctl_init(){
#define _(n, p, w) (DDR ## p) |= _BV(w);
  CTL
#undef _
}

#endif
