ifeq ($(menu),on)
  ifdef menu.items
    def+=MENU_ITEMS='$(call parsespec,$(menu.items))'
  endif
  ifdef menu.ops
    def+=MENU_OPS='$(call parsespec,$(menu.ops))'
  endif
  ifneq ($(findstring cyclic,$(menu.features)),)
    def+=MENU_CYCLIC=1
  endif
  ifneq ($(findstring ticks,$(menu.features)),)
    def+=MENU_TICKS=1
  endif
  ifdef menu.autoreset
    def+=MENU_AUTO_RESET=$(menu.autoreset)
  endif
endif
