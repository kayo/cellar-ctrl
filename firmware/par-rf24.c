#include "par.h"
#include "par-rf24.h"

#include "par-proto.h"
#include "rf24.h"

#include <stddef.h>
#include <string.h>

static rf24_addr_t _rf24_addr_ = {
 _5: { PAR_RF24_ADDR }
};

#ifdef PAR_RF24_AES
#  include "aes.h"

static uint8_t _aes_key_[] = {
  PAR_RF24_AES_KEY
};

//#  define PAR_RF24_AES_BITS (sizeof(_aes_key_) << 3)
#  define PAR_RF24_AES_BLOCK (128 >> 3)

#  define _AES_PFX_(bits, name) aes ## bits ## _ ## name
#  define _AES_PFX(bits, name) _AES_PFX_(bits, name)
#  define _AES(name) _AES_PFX(PAR_RF24_AES_BITS, name)

static _AES(ctx_t) _aes_ctx_;
#endif

static rf24_val_t _rf24_writing_;
static rf24_dev_t _rf24_device_;

#define dev (&_rf24_device_)

void par_rf24_init(){
  rf24_open(dev, 0, 0);
  
  rf24_set_power(dev, 1);
  
  //rf24_set_payload_size(dev, 32);
  rf24_set_dynamic_payloads(dev, 1);
  
  rf24_set_ack_payload(dev, 1);
  rf24_set_auto_ack(dev, 1);
  
  rf24_open_reading_pipe(dev, 1, &_rf24_addr_);
  rf24_set_listen(dev, 1);
  
#ifdef PAR_RF24_AES
  _AES(init)(_aes_key_, &_aes_ctx_);
#endif

  _rf24_writing_ = 0;
}

void par_rf24_poll(){
  if(_rf24_writing_){
    rf24_write_stat_t stats;
    rf24_write_poll_t status = rf24_write_poll(dev, &stats);
    
    //rf24_queue_stat_t qs = rf24_get_queue_stat(dev);
    
    if(status != RF24_WRITE_ACTIVE){
      rf24_open_reading_pipe(dev, 1, &_rf24_addr_);
      rf24_set_listen(dev, 1);
      
      _rf24_writing_ = 0;
    }
  }else{
    if(rf24_read_poll(dev, NULL)){
      rf24_len_t len = RF24_PAYLOAD_SIZE_MAX;
      rf24_val_t raw[RF24_PAYLOAD_SIZE_MAX];
      
      rf24_read(dev, raw, &len);
      
      /* use simple ECB (Electronic Code Book) mode */
#ifdef PAR_RF24_AES
      //rf24_val_t p; /* partial block */
      rf24_len_t i; /* iterator */
      
      for(i = 0; i < len; i += PAR_RF24_AES_BLOCK){
        _AES(dec)(raw + i, &_aes_ctx_);
      }
#endif
      
      raw[0] = par_proto_exec(raw + 1, raw[0] - 1) + 1;
      len = raw[0];
      
      /* use simple ECB (Electronic Code Book) mode */
#ifdef PAR_RF24_AES
      for(i = 0; i < len; i += PAR_RF24_AES_BLOCK){
        _AES(enc)(raw + i, &_aes_ctx_);
      }
      
      len = i;
#endif
      
      rf24_set_listen(dev, 0);
      rf24_open_writing_pipe(dev, &_rf24_addr_);
      
      rf24_write(dev, raw, len);
      _rf24_writing_ = 1;
    }
  }
}
