#include"par-proto.h"

#include<string.h>
#include<avr/pgmspace.h>   /* required by usbdrv.h */
#include<util/delay.h>
#include<avr/interrupt.h>
#include"usbdrv.h"
#include"oddebug.h"        /* This is also an example for using debug macros */

const PROGMEM char usbHidReportDescriptor[22] = {    /* USB report descriptor */
  0x06, 0x00, 0xff,              // USAGE_PAGE (Generic Desktop)
  0x09, 0x01,                    // USAGE (Vendor Usage 1)
  0xa1, 0x01,                    // COLLECTION (Application)
  0x15, 0x00,                    //   LOGICAL_MINIMUM (0)
  0x26, 0xff, 0x00,              //   LOGICAL_MAXIMUM (255)
  0x75, 0x08,                    //   REPORT_SIZE (8)
  0x95, 0x80,                    //   REPORT_COUNT (128)
  0x09, 0x00,                    //   USAGE (Undefined)
  0xb2, 0x02, 0x01,              //   FEATURE (Data,Var,Abs,Buf)
  0xc0                           // END_COLLECTION
};

static uchar proto_buf[128], proto_pos = 0, proto_len = 0;

/* The following variables store the status of the current data transfer */

/* usbFunctionRead() is called when the host requests a chunk of data from
 * the device. For more information see the documentation in usbdrv/usbdrv.h.
 */
uchar usbFunctionRead(uchar *data, uchar len) {
  if(proto_pos < proto_len){
    if(len > proto_len - proto_pos){
      len = proto_len - proto_pos;
    }
    memcpy(data, proto_buf + proto_pos, len);
    proto_pos += len;
    return len;
  }
  return 0;
}

/* usbFunctionWrite() is called when the host sends a chunk of data to the
 * device. For more information see the documentation in usbdrv/usbdrv.h.
 */

static usbWord_t proto_cnt;

uchar usbFunctionWrite(uchar *data, uchar len) {
  if(len > 0 && proto_len < sizeof(proto_buf)){
    if(len > sizeof(proto_buf) - proto_len){
      len = sizeof(proto_buf) - proto_len;
    }
    memcpy(proto_buf + proto_len, data, len);
    proto_len += len;
  }
  if(proto_len < proto_cnt.word){
    return 0;
  }else{
    return 1;
  }
}

usbMsgLen_t usbFunctionSetup(uchar data[8]) {
  usbRequest_t *rq = (void *)data;
  
  if((rq->bmRequestType & USBRQ_TYPE_MASK) == USBRQ_TYPE_CLASS){    /* HID class request */
    if(rq->wValue.bytes[0] != 0){
      if(rq->bRequest == USBRQ_HID_GET_REPORT){  /* wValue: ReportType (highbyte), ReportID (lowbyte) */
        /* since we have only one report type, we can ignore the report-ID */
        proto_len = par_proto_exec(proto_buf, proto_len);
        proto_pos = 0;
        return USB_NO_MSG;  /* use usbFunctionRead() to obtain data */
      }else if(rq->bRequest == USBRQ_HID_SET_REPORT){
        /* since we have only one report type, we can ignore the report-ID */
        proto_cnt = rq->wLength;
        proto_len = 0;
        return USB_NO_MSG;  /* use usbFunctionWrite() to receive data from host */
      }
    }
  }else{
    /* ignore vendor type requests, we don't use any */
  }
  return 0;
}

void par_vusb_init(){
  odDebugInit();
  DBG1(0x00, 0, 0);
  usbInit();
  usbDeviceDisconnect();
  _delay_ms(250);
  usbDeviceConnect();
  sei();
  DBG1(0x01, 0, 0);
}

void par_vusb_poll(){
  DBG1(0x02, 0, 0);
  usbPoll();
}

/*
par_ev(vusb) {
  
}
*/
