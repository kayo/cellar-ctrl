#ifndef __par_proto_h__
#define __par_proto_h__ "par-proto.h"

#include <stdint.h>

#define PAR_PROTO_BUFFER_LEN 128

#ifdef PAR
#  define PAR_PROTO_SERVER
#else
#  define PAR_PROTO_CLIENT
#endif

typedef uint8_t par_len_t;
typedef uint8_t par_idx_t;
typedef uint8_t par_val_t;
typedef void par_raw_t;

#define PAR_PROTO_NO_PAR ((par_idx_t)-1)

#ifdef PAR_PROTO_SERVER

par_len_t /*len*/ par_proto_exec(par_raw_t *buf, par_len_t len);

#endif//PAR_PROTO_SERVER

#ifdef PAR_PROTO_CLIENT

typedef struct _par_inf_ par_inf_t;
typedef /*enum _par_ret_*/ uint8_t par_ret_t;
typedef struct _par_dev_ par_dev_t;
typedef struct _par_hif_ par_hif_t;

const char* par_ret_str(par_dev_t *dev, par_ret_t ret);
#define par_online(dev) ((dev)->_hif ? 1 : 0)

#define par_get_udata(dev) ((dev)->udata)
#define par_set_udata(dev, ud) {(dev)->udata = ud;}

//#define par_new() (malloc(sizeof(par_dev_t)))
par_dev_t *par_new();
//#define par_del(dev) free(dev);
void par_del(par_dev_t *dev);

/* device name looks like this host_iface[+host_params]:device_name[+device_params] */
/* examples: usb[+hid]:device-name, rf24[+num]:device-pipe-mac, ... */
par_ret_t par_open(par_dev_t *dev, const char *uri);
par_ret_t par_close(par_dev_t *dev);

par_ret_t par_test(par_dev_t *dev);

par_ret_t par_load(par_dev_t *dev);
par_ret_t par_save(par_dev_t *dev);

par_len_t par_list(par_dev_t *dev, const par_inf_t **info);
par_idx_t par_find(par_dev_t *dev, const char *name);
const par_inf_t *par_info(par_dev_t *dev, par_idx_t par);

par_ret_t par_get_raw(par_dev_t *dev, par_idx_t par, par_raw_t **val, par_len_t *len);
par_ret_t par_set_raw(par_dev_t *dev, par_idx_t par, const par_raw_t *val, par_len_t len);

par_ret_t par_get_str(par_dev_t *dev, par_idx_t par, char **val);
par_ret_t par_set_str(par_dev_t *dev, par_idx_t par, const char *val);

par_ret_t par_def(par_dev_t *dev, par_idx_t par);
#define par_reset par_def

enum _par_ret_ {
  PAR_OK = 0,
  PAR_ERR_OPEN,
  PAR_ERR_SEND,
  PAR_ERR_RECV,
  PAR_ERR_CHECK,
  PAR_ERR_PROTO,
  PAR_ERR_TYPE,
  PAR_ERR_SIZE,
  PAR_ERR_NOPAR,
  PAR_ERR_NOHIF,
  PAR_DEV_CLOSED,
  PAR_DEV_OPENED
};

struct _par_inf_ {
  char *name;
  char *type;
  char *hint;
};

struct _par_dev_ {
  /* implement */
  par_raw_t *_dev;
  par_hif_t *_hif; /* host interface */
  par_val_t _buf[PAR_PROTO_BUFFER_LEN];
  par_len_t _retry;
  /* internals */
  par_inf_t *infos;
  par_len_t count;
  /* externals */
  par_raw_t *udata;
};

#endif//PAR_PROTO_CLIENT

#endif
