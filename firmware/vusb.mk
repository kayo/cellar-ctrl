# VUSB
# Converting functions
conv.usbid=$(shell printf $(1) | sed -r -e 's/([0-9a-f]{2})([0-9a-f]{2})/0x\2,0x\1/g')
conv.usbname="$(shell printf $(1) | sed -r -e "s/(\S)/,\\'\1\\'/g" -e "s/^,//g")"
conv.usblen=$(shell printf $(1) | wc -c)
conv.usbver=$(shell printf '0x%.2x,0x%.2x' $(lastword $(subst ., ,$(1))) $(firstword $(subst ., ,$(1))))

-include ../config

ifeq ($(vusb),on)
  ifneq ($(vusb.drv),static)
    vusb.obj+=usbdrv.o
  else
    def+=USB_PUBLIC=static
  endif
  # for #include "usbdrv.{c,h}"
  cc.dir+=$(vusb.path)/usbdrv
  vusb.obj+=usbdrvasm.o
  ifneq ($(vusb.debug),)
    def+=DEBUG_LEVEL=$(vusb.debug)
    vusb.obj+=oddebug.o
  endif
  # for #include "usbconfig.h"
  cc.dir+=.
  obj+=$(addprefix $(vusb.path)/usbdrv/,$(vusb.obj))
  # USB IDs
  USBDEFS:= \
USB_CFG_VENDOR_ID=$(call conv.usbid,$(vusb.vendor.id)) \
USB_CFG_DEVICE_ID=$(call conv.usbid,$(vusb.device.id)) \
USB_CFG_DEVICE_VERSION=$(call conv.usbver,$(vusb.device.version)) \
USB_CFG_VENDOR_NAME=$(call conv.usbname,$(vusb.vendor.name)) \
USB_CFG_VENDOR_NAME_LEN=$(call conv.usblen,$(vusb.vendor.name)) \
USB_CFG_DEVICE_NAME=$(call conv.usbname,$(vusb.device.name)) \
USB_CFG_DEVICE_NAME_LEN=$(call conv.usblen,$(vusb.device.name))
  # USB I/O
  USBDEFS+= \
USB_CFG_IOPORTNAME=$(vusb.port) \
USB_CFG_DMINUS_BIT=$(vusb.minus) \
USB_CFG_DPLUS_BIT=$(vusb.plus)
  # Add defs
  def+=$(USBDEFS)
endif

ifneq ($(vusb),)
  prepare: get-vusb
  get-vusb:
	@last=`wget -O - '$(vusb.url)$(vusb.idx)' | sed 's/["]/\n\0\n/g' | grep '.tar.gz' | head -n1`; name=`echo $$last | sed -r 's/^.*\/([^\/]+)\.tar\.gz$$/\1/g'`; file=$$name.tar.gz; [ -d "$$name" ] || { wget -c "$(vusb.url)$$last"; tar -C "$(dir $(vusb.path))" -xf "$$file"; rm -f "$$file" "$(vusb.path)"; ln -s "$$name" "$(vusb.path)"; }
endif
