#include "spi.h"

#ifdef __AVR__

#include <avr/io.h>

#define CAT(a, b) a ## b
//#define CAT(a, b) _CAT(a, b)
#define DDR(port) CAT(DDR, port)
#define PORT(port) CAT(PORT, port)

spi_ret_t spi_open(spi_dev_t *dev, spi_hwid_t hwid, spi_mode_t mode, spi_bits_t bits, spi_rate_t rate){
  DDR(SPI_PORT) |= _BV(SPI_MOSI) | _BV(SPI_SCK);
  DDR(SPI_PORT) &= ~_BV(SPI_MISO);
  
  SPCR |= _BV(MSTR);
  
  dev->mode = mode;
  if(dev->mode & SPI_CLK_POLARITY){
    SPCR |= _BV(CPOL);
  }else{
    SPCR &= ~_BV(CPOL);
  }
  
  if(dev->mode & SPI_CLK_PHASE){
    SPCR |= _BV(CPHA);
  }else{
    SPCR &= ~_BV(CPHA);
  }
  
  if(dev->mode & SPI_LSB_FRST){
    SPCR |= _BV(DORD);
  }else{
    SPCR &= ~_BV(DORD);
  }
  
  dev->bits = bits;
  dev->rate = rate;

#define _SPI_RATE_DIV(div) (dev->rate >= (F_CPU/(div)))
  
  if(_SPI_RATE_DIV(2)){
    SPCR &= ~(_BV(SPR1) | _BV(SPR0));
    SPSR |= _BV(SPI2X);
  }else if(_SPI_RATE_DIV(4)){
    SPCR &= ~(_BV(SPR1) | _BV(SPR0));
    SPSR &= ~_BV(SPI2X);
  }else if(_SPI_RATE_DIV(8)){
    SPCR &= ~_BV(SPR1);
    SPCR |= _BV(SPR0);
    SPSR |= _BV(SPI2X);
  }else if(_SPI_RATE_DIV(16)){
    SPCR &= ~_BV(SPR1);
    SPCR |= _BV(SPR0);
    SPSR &= ~_BV(SPI2X);
  }else if(_SPI_RATE_DIV(32)){
    SPCR |= _BV(SPR1);
    SPCR &= ~_BV(SPR0);
    SPSR |= _BV(SPI2X);
  }else if(_SPI_RATE_DIV(64)){
    SPCR |= _BV(SPR1) | _BV(SPR0);
    SPSR |= _BV(SPI2X);
  }else if(_SPI_RATE_DIV(128)){
    SPCR |= _BV(SPR1) | _BV(SPR0);
    SPSR &= ~_BV(SPI2X);
  }else{
    return SPI_CANT_SET_RATE;
  }

  if(!(dev->mode & SPI_CSN_NO)){
    DDR(SPI_PORT) |= _BV(SPI_CSN);
    
    if(dev->mode & SPI_CSN_HI){
      PORT(SPI_PORT) &= ~_BV(SPI_CSN);
    }else{
      PORT(SPI_PORT) |= _BV(SPI_CSN);
    }
  }
  
  SPCR |= _BV(SPE);
  
  return SPI_NO_ERROR;
}

spi_ret_t spi_close(spi_dev_t *dev){
  SPCR &= ~_BV(SPE);
  
  if(!(dev->mode & SPI_CSN_NO)){
    if(dev->mode & SPI_CSN_HI){
      PORT(SPI_PORT) |= _BV(SPI_CSN);
    }else{
      PORT(SPI_PORT) &= ~_BV(SPI_CSN);
    }
  }
  
  if(!(dev->mode & SPI_CSN_NO)){
    DDR(SPI_PORT) &= ~_BV(SPI_CSN);
  }
  
  DDR(SPI_PORT) &= ~(_BV(SPI_MOSI) | _BV(SPI_SCK));
  //DDR(SPI_PORT) |= _BV(SPI_MISO);
  
  return SPI_NO_ERROR;
}

static inline void _spi_busy_wait(spi_dev_t *dev){
  for(; !(SPSR & _BV(SPIF)); );
}

spi_ret_t spi_transfer(spi_dev_t *dev, const spi_val_t *tx, spi_val_t *rx, spi_len_t len){
  if(!(dev->mode & SPI_CSN_NO)){
    if(dev->mode & SPI_CSN_HI){
      PORT(SPI_PORT) |= _BV(SPI_CSN);
    }else{
      PORT(SPI_PORT) &= ~_BV(SPI_CSN);
    }
  }
  
  for(spi_len_t i = 0; i < len; i++){
    SPDR = tx ? tx[i] : 0xff;
    _spi_busy_wait(dev);
    if(rx){
      rx[i] = SPDR;
    }
  }
  
  if(!(dev->mode & SPI_CSN_NO)){
    if(dev->mode & SPI_CSN_HI){
      PORT(SPI_PORT) &= ~_BV(SPI_CSN);
    }else{
      PORT(SPI_PORT) |= _BV(SPI_CSN);
    }
  }
  
  return SPI_NO_ERROR;
}

#endif
