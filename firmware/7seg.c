#ifdef SEVSEG

#include"7seg.h"

#include<avr/io.h>
#include<string.h>

#if !defined(SEVSEG_TYPE)
#  error "Please define indicator type SEVSEG_COMMON_ANODE or SEVSEG_COMMON_CATODE!"
#endif

#if !defined(SEVSEG_SEGMENTS)
#  error "Please define indicator segments! Each segment must be defined as _(name, port, pin), where: name = a-g or p; port = A,B,C,D; pin = 0-7. For example: SEVSEG_SEGMENTS='_(a,D,1)_(b,D,2)_(c,A,1)....'!"
#endif

#if !defined(SEVSEG_DIGITS)
#  error "Please define indicator digits! Each digit must be defined as _(pos, port, pin), where: pos = 0-N; port = A,B,C,D; pin = 0-7. For example: SEVSEG_SEGMENTS='_(a,D,1)_(b,D,2)_(c,A,1)....'!"
#endif

#if SEVSEG_PROGMEM == 1
#  include <avr/pgmspace.h>
#  define SS_GET(v) (pgm_read_byte(&(v)))
#else
#  define SS_GET(v) (v)
#  define PROGMEM
#endif

#if SEVSEG_TYPE == SEVSEG_COMMON_ANODE
#define SS_S(p, w) PORT ## p &= ~_BV(w)
#define SS_R(p, w) PORT ## p |= _BV(w)
#endif

#if SEVSEG_TYPE == SEVSEG_COMMON_CATODE
#define SS_S(p, w) PORT ## p |= _BV(w)
#define SS_R(p, w) PORT ## p &= ~_BV(w)
#endif

#define SS_O(p, w) DDR ## p |= _BV(w)
#define SS_I(p, w) DDR ## p &= ~_BV(w)

#define SS_DATA_CODE(c) ((c >= ss_from && c < (ss_from + ss_leng)) ? SS_GET(ss_tab[c - ss_from]) : SS_N)

#define SS_N SEVSEG_N
#define SS_P SEVSEG_P
#define SS_A SEVSEG_A
#define SS_B SEVSEG_B
#define SS_C SEVSEG_C
#define SS_D SEVSEG_D
#define SS_E SEVSEG_E
#define SS_F SEVSEG_F
#define SS_G SEVSEG_G

#define ss_from 0x20
#define ss_leng 41
static const unsigned char ss_tab[] PROGMEM = {
  SS_N, // 20
  SS_B | SS_P, // 21 !
  SS_B | SS_F, // 22
  SS_N, // 23 #
  SS_A | SS_F | SS_G | SS_C | SS_D, // 24 $
  SS_C | SS_D | SS_E | SS_G, // 2A, // 25 %
  SS_N, // 26 #
  SS_B, // 27 '
  SS_A | SS_D | SS_E | SS_F, // 28 (
  SS_A | SS_B | SS_C | SS_D, // 29 )
  SS_A | SS_B | SS_G | SS_F, // 2A *
  SS_N, // 2B +
  SS_C, // 2C ,
  SS_G, // 2D -
  SS_P, // 2E .
  SS_E | SS_F, // 2F /
  SS_A | SS_B | SS_C | SS_D | SS_E | SS_F, // 30 0
  SS_B | SS_C, // 31 1
  SS_A | SS_B | SS_G | SS_E | SS_D, // 32 2
  SS_A | SS_B | SS_G | SS_C | SS_D, // 33 3
  SS_B | SS_C | SS_F | SS_G, // 34 4
  SS_A | SS_F | SS_G | SS_C | SS_D, // 35 5
  SS_A | SS_F | SS_E | SS_D | SS_C | SS_G, // 36 6
  SS_A | SS_B | SS_C, // 37 7
  SS_A | SS_B | SS_C | SS_D | SS_E | SS_F | SS_G, // 30 8
  SS_G | SS_F | SS_A | SS_B | SS_C | SS_D, // 31 9
  SS_N, // 32 :
  SS_N, // 33 ;
  SS_E | SS_G, // 34 <
  SS_D | SS_G, // 35 =
  SS_C | SS_D, // 36 >
  SS_F | SS_A | SS_B | SS_P, // 37 ?
  SS_A | SS_B | SS_C | SS_D | SS_E | SS_G, // 30 @
  SS_E | SS_F | SS_A | SS_B | SS_C | SS_G, // 31 A
  SS_F | SS_E | SS_D | SS_C | SS_G, // 32 B
  SS_A | SS_F | SS_E | SS_D, // 33 C
  SS_B | SS_C | SS_D | SS_E | SS_G, // 34 D
  SS_A | SS_F | SS_G | SS_E | SS_D, // 35 E
  SS_A | SS_F | SS_G | SS_E, // 36 F
  SS_A | SS_F | SS_E | SS_D | SS_C, // 37 G
  SS_F | SS_G | SS_E | SS_B | SS_C  // 37 H
};

#define _(n,p,w) + 1 
static unsigned char ss_buf[0 SEVSEG_DIGITS] = {
#undef _
#define _(n,p,w) SS_N,
  SEVSEG_DIGITS
#undef _
};
#define SS_DIGITS sizeof(ss_buf)
static unsigned char ss_cur = 0, ss_pre = SS_DIGITS - 1;
static unsigned char ss_state = 0;

void sevseg_enable(){
  ss_state = 1;
#define _(n,p,w) DDR ## p |= _BV(w);
  SEVSEG_SEGMENTS SEVSEG_DIGITS // Config ports to output
#undef _
}

void sevseg_disable(){
  ss_state = 0;
#define _(n,p,w) DDR ## p &= ~_BV(w);
  SEVSEG_SEGMENTS SEVSEG_DIGITS // Deconf ports
#undef _
}

void sevseg_update(){
  if(ss_state == 0)return;
  
#define _(n,p,w) if(ss_pre == n) SS_S(p, w);
  SEVSEG_DIGITS;
#undef _

#define _(n,p,w) if(ss_cur == n) SS_R(p, w);
  SEVSEG_DIGITS;
#undef _

#define _(n,p,w) if(ss_buf[ss_cur] & _BV(SEVSEG_##n)) SS_S(p, w); else SS_R(p, w);
  SEVSEG_SEGMENTS;
#undef _
  
  ss_pre = ss_cur;
  
  if(ss_cur < SS_DIGITS - 1){
    ss_cur ++;
  }else{
    ss_cur = 0;
  }
}

static unsigned char ss_beg = 0, ss_len = SS_DIGITS;

void sevseg_window(unsigned char beg, unsigned char len){
  (beg >= SS_DIGITS) && (beg = SS_DIGITS - 1);
  if((beg + len) > SS_DIGITS){
    len = SS_DIGITS - beg;
  }
  if(len == 0){
    len = SS_DIGITS - beg;
  }
  ss_beg = beg;
  ss_len = len;
}

void sevseg_print_(const unsigned char *buf){
  unsigned char i, j, c, l = ss_len + ss_beg;
  for(i = 0, j = ss_beg; j < l; j++){
    c = buf[i] ? SS_DATA_CODE(buf[i]) : SS_N;
    if(buf[i]){
      i++;
    }
    if(c == SS_P){
      j--;
      ss_buf[j] = ss_buf[j] | c;
    }else{
      ss_buf[j] = c;
    }
  }
}

void sevseg_output(const unsigned char *buf){
  memcpy(ss_buf + ss_beg, buf, ss_len);
}

#endif
