ifneq ($(findstring usb,$(par.hifs)),)

-include ../config

USBDEFS:= \
USB_VENDOR_ID=0x$(vusb.vendor.id) \
USB_DEVICE_ID=0x$(vusb.device.id) \
USB_DEVICE_VERSION=$(vusb.device.version) \
USB_VENDOR_NAME=\"$(vusb.vendor.name)\" \
USB_DEVICE_NAME=\"$(vusb.device.name)\"

def+=$(USBDEFS)

endif
