#ifdef PAR_PROTO_HIF_usb

#include "par-proto.h"

#define PAR_PROTO_HIF usb

#include "par-proto-hif.h"

#include "hiddata.h"

#include <stdlib.h>

PAR_PROTO_HIF_STRUCT(usbDevice_t *dev; int err);

PAR_PROTO_HIF_NEW_DEL();

/* usb:device_name */

PAR_PROTO_HIF_OPEN(){
  dev->dev = NULL;
  return (dev->err = usbhidOpenDevice(&dev->dev, USB_VENDOR_ID, USB_VENDOR_NAME, USB_DEVICE_ID, name ? name + 1 : USB_DEVICE_NAME)) ? 0 : 1;
}

PAR_PROTO_HIF_CLOS(){
  usbhidCloseDevice(dev->dev);
  dev->dev = NULL;
}

PAR_PROTO_HIF_SEND(){
  return (dev->err = usbhidSetReport(dev->dev, (const char*)buf, len)) ? 0 : 1;
}

PAR_PROTO_HIF_RECV(){
  int _len = *len;
  dev->err = usbhidGetReport(dev->dev, (char*)buf, &_len);
  *len = _len;
  return dev->err ? 0 : 1;
}

PAR_PROTO_HIF_ERST(){
  switch(dev->err){
  case 0: return NULL;
  case USBOPEN_ERR_ACCESS: return "Access to device denied";
  case USBOPEN_ERR_NOTFOUND: return "The specified device was not found";
  case USBOPEN_ERR_IO: return "Communication error with device";
  }
  return "Unknown USB error";
}

PAR_PROTO_HIF_REGISTER();

#endif//PAR_PROTO_HIF_usb
