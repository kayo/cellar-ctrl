#ifdef PAR_PROTO_HIF_rf24

#include "par-proto.h"

#define PAR_PROTO_HIF rf24

#include "par-proto-hif.h"

#include "aes.h"

#ifdef PAR_RF24_SHA_HASH
#  ifdef SHA_INTERN
#    include "sha256.h"
#  else
#    include <openssl/sha.h>

static inline void sha256(void *dig, const uint8_t *raw, uint8_t len){
  SHA256_CTX ctx;
  SHA256_Init(&ctx);
  SHA256_Update(&ctx, raw, len);
  SHA256_Final(dig, &ctx);
}

#  endif
#endif

typedef aes256_ctx_t aes_ctx_t;

#define PAR_RF24_AES_BLOCK (128 >> 3)

#include "rf24.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* TODO: Multiple Interfaces */
static par_val_t used = 0;
static rf24_dev_t radio;

#define dev (&radio)
static void init_radio(){
  if(used++ < 1){
    rf24_open(dev, PAR_RF24_SPI_DEV, PAR_RF24_CE_PIN);
    
    rf24_set_power(dev, 1);
    
    //rf24_set_payload_size(dev, 32);
    rf24_set_dynamic_payloads(dev, 1);
    
    rf24_set_ack_payload(dev, 1);
    rf24_set_auto_ack(dev, 1);
    
    rf24_set_listen(dev, 1);
  }
}
static void free_radio(){
  if(used > 0 && --used < 1){
    rf24_set_power(dev, 0);
    
    rf24_close(dev);
  }
}
#undef dev

PAR_PROTO_HIF_STRUCT(rf24_dev_t *dev; rf24_addr_t mac; rf24_channel_t ch; rf24_data_rate_t dr; aes_ctx_t aes_ctx; par_val_t aes);

PAR_PROTO_HIF_NEW_DEL();

static inline par_len_t aes_rounds(par_len_t len){
  switch(len){
  case 16: return 10;
  case 24: return 12;
  case 32: return 14;
  }
  return 0;
}

/* rf24 uri: rf24[#N_retry]:mac_addr[@channel][&data_rate][!aes_key|?sha_pwd] */

static char hex2int(char c){
  if(c >= '0' && c <= '9'){
    return c - '0';
  }else if(c >= 'A' && c <= 'F'){
    return c - 'A' + 10;
  }else if(c >= 'a' && c <= 'f'){
    return c - 'a' + 10;
  }
  return -1;
}

static const char* hex2val(const char* str, uint8_t *val){
  char h = hex2int(str[0]);
  if(h < 0){
    return str;
  }
  char l = hex2int(str[1]);
  if(l < 0){
    return str;
  }
  if(val){
    *val = (h << 4) | l;
  }
  return str + 2;
}

PAR_PROTO_HIF_OPEN(){
  init_radio();
  
  dev->dev = &radio;
  
  par_len_t i = 0;
  char *p, *t;
  
  p = (char*)name + 1;
  for(i = 0; i < 5; i++){ // parse mac
    t = hex2val(p, &dev->mac._5[i]);
    if(p == t){
      break;
    }
    p = t;
    if(!p){
      return 0;
    }
    if(p[0] == ','){
      p++;
    }
    if(p[0] == '\0' && p[0] == '@' || p[0] == '&' || p[0] == '!' || p[0] == '?'){
      break;
    }
  }
  
  dev->ch = 0;
  
  if(p[0] == '@'){ // parse channel
    p++;
    dev->ch = strtoul(p, &t, 10);
    if(t == p){
      return 0;
    }
    p = t;
  }
  
  dev->dr = RF24_DATA_RATE_1MBS;
  
  if(p[0] == '&'){ // parse data rate
    p++;
    uint8_t dr = strtoul(p, &t, 10);
    if(t == p){
      return 0;
    }
    p = t;
    switch(dr){
    case 1: dev->dr = RF24_DATA_RATE_1MBS; break;
    case 2: dev->dr = RF24_DATA_RATE_2MBS; break;
    case 250: dev->dr = RF24_DATA_RATE_250KBS; break;
    }
    for(; p[0] != '\0' && p[0] != '!' && p[0] != '?'; p++);
  }
  
  uint8_t key[32];
  
#ifdef PAR_RF24_SHA_HASH
  if(p[0] == '?'){
    p++;
    sha256(&key, p, strlen(p));
    i = sizeof(key);
  }
#endif
  
  if(!i && p[0] == '!'){
    p++;
    for(; i < 32; i++){
      t = hex2val(p, &key[i]);
      if(t == p){
        break;
      }
      p = t;
      if(p[0] == '\0'){
        i++;
        break;
      }
      if(p[0] == ','){
        p++;
      }
    }
  }
  
  if(i){
    if(i == 16 || i == 24 || i == 32){
      dev->aes = i;
      aes_init(key, i << 3, (aes_genctx_t *)&dev->aes_ctx);
    }else{
      return 0;
    }
  }else{
    dev->aes = 0;
  }
  
  //fprintf(stderr, "%x %x\n", dev->mac._5[4], key[15]);
  
  return 1;
}

PAR_PROTO_HIF_CLOS(){
  free_radio();
}

PAR_PROTO_HIF_SEND(){
  rf24_set_channel(dev->dev, dev->ch);
  rf24_set_data_rate(dev->dev, dev->dr);
  
  rf24_val_t *raw = (rf24_raw_t*)buf;
  
  raw[0] = len;
  
  if(dev->aes){
    rf24_len_t i;
    for(i = 0; i < len; i += PAR_RF24_AES_BLOCK){
      aes_encrypt_core((aes_cipher_state_t *)(raw + i), (aes_genctx_t *)&dev->aes_ctx, aes_rounds(dev->aes));
    }
    len = i;
  }
  
  rf24_set_listen(dev->dev, 0);
  rf24_open_writing_pipe(dev->dev, &dev->mac);
  
  if(RF24_WRITE_FAIL == rf24_write_with_wait(dev->dev, raw, len, NULL)){
    return 0;
  }
  
  return 1;
}

PAR_PROTO_HIF_RECV(){
  rf24_open_reading_pipe(dev->dev, 1, &dev->mac);
  rf24_set_listen(dev->dev, 1);
  
  rf24_val_t *raw = buf;
  
  rf24_read_with_wait(dev->dev, raw, len, NULL, 1000);
  
  if(dev->aes){
    for(rf24_len_t i = 0; i < *len; i += PAR_RF24_AES_BLOCK){
      aes_decrypt_core((aes_cipher_state_t *)(raw + i), (aes_genctx_t *)&dev->aes_ctx, aes_rounds(dev->aes));
    }
  }
  
  *len = raw[0];

  return 1;
}

PAR_PROTO_HIF_ERST(){
  return NULL;
}

PAR_PROTO_HIF_REGISTER();

#endif//PAR_PROTO_HIF_usb
