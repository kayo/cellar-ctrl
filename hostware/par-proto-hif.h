/* host interface functions */

typedef void/*ok*/(*par_new_f)(par_raw_t **dev);
typedef void/*ok*/(*par_del_f)(par_raw_t **dev);

typedef par_val_t/*ok*/(*par_open_f)(par_raw_t *dev, const char *name);
typedef par_val_t/*ok*/(*par_clos_f)(par_raw_t *dev);

typedef par_val_t/*ok*/(*par_send_f)(par_raw_t *dev, const par_raw_t *buf, par_len_t len);
typedef par_val_t/*ok*/(*par_recv_f)(par_raw_t *dev, par_raw_t *buf, par_len_t *len);
typedef const char*/*error*/(*par_erst_f)(par_raw_t *dev); /* error string */

/* host interface descriptor struct */

struct _par_hif_ {
  const char* type;
  par_new_f new;
  par_del_f del;
  par_open_f open;
  par_clos_f clos;
  par_send_f send;
  par_recv_f recv;
  par_erst_f erst;
};

#define _PAR_PROTO_HIF_SYMBOL(type, name)       \
  _par_hif_ ## type ## _ ## name

#define _PAR_PROTO_HIF_METHOD(type, name, ret, arg...) \
  static ret _PAR_PROTO_HIF_SYMBOL(type, name)         \
       (_PAR_PROTO_HIF_SYMBOL(type, dev_t) arg)

#define _PAR_PROTO_HIF_STRUCT(type, fields)                     \
  typedef struct _PAR_PROTO_HIF_SYMBOL(type, dev)               \
  _PAR_PROTO_HIF_SYMBOL(type, dev_t);                           \
  struct _PAR_PROTO_HIF_SYMBOL(type, dev) {                     \
    fields;                                                     \
  }

#define _PAR_PROTO_HIF_NEW_DEL(type)                            \
  _PAR_PROTO_HIF_METHOD(type, new, void, **dev) {               \
    *dev = malloc(sizeof(_PAR_PROTO_HIF_SYMBOL(type, dev_t)));  \
  }                                                             \
  _PAR_PROTO_HIF_METHOD(type, del, void, **dev) {               \
    free(*dev);                                                 \
    *dev = NULL;                                                \
  }

#define _PAR_PROTO_HIF_METHOD_REF(type, name)             \
  (par_ ## name ## _f) _PAR_PROTO_HIF_SYMBOL(type, name)

#define _PAR_PROTO_HIF_TYPENAME(type) #type

#define _PAR_PROTO_HIF_REGISTER(type) par_hif_t \
  _PAR_PROTO_HIF_SYMBOL(type, reg) = {          \
    _PAR_PROTO_HIF_TYPENAME(type),              \
    _PAR_PROTO_HIF_METHOD_REF(type, new),       \
    _PAR_PROTO_HIF_METHOD_REF(type, del),       \
    _PAR_PROTO_HIF_METHOD_REF(type, open),      \
    _PAR_PROTO_HIF_METHOD_REF(type, clos),      \
    _PAR_PROTO_HIF_METHOD_REF(type, send),      \
    _PAR_PROTO_HIF_METHOD_REF(type, recv),      \
    _PAR_PROTO_HIF_METHOD_REF(type, erst)       \
  }

#define _PAR_PROTO_HIF_EXTERN(type)                   \
  extern par_hif_t _PAR_PROTO_HIF_SYMBOL(type, reg);

#ifdef PAR_PROTO_HIF

#define PAR_PROTO_HIF_SYMBOL(type, name) _PAR_PROTO_HIF_SYMBOL(PAR_PROTO_HIF, name)

#define PAR_PROTO_HIF_STRUCT(fields) _PAR_PROTO_HIF_STRUCT(PAR_PROTO_HIF, fields)

#define PAR_PROTO_HIF_NEW_DEL() _PAR_PROTO_HIF_NEW_DEL(PAR_PROTO_HIF)

#define PAR_PROTO_HIF_OPEN() _PAR_PROTO_HIF_METHOD(PAR_PROTO_HIF, open, par_val_t, *dev, const char* name)

#define PAR_PROTO_HIF_CLOS() _PAR_PROTO_HIF_METHOD(PAR_PROTO_HIF, clos, par_val_t, *dev)

#define PAR_PROTO_HIF_SEND() _PAR_PROTO_HIF_METHOD(PAR_PROTO_HIF, send, par_val_t, *dev, const par_raw_t *buf, par_len_t len)

#define PAR_PROTO_HIF_RECV() _PAR_PROTO_HIF_METHOD(PAR_PROTO_HIF, recv, par_val_t, *dev, par_raw_t *buf, par_len_t *len)

#define PAR_PROTO_HIF_ERST() _PAR_PROTO_HIF_METHOD(PAR_PROTO_HIF, erst, const char*, *dev)

#define PAR_PROTO_HIF_REGISTER() _PAR_PROTO_HIF_REGISTER(PAR_PROTO_HIF)

#endif//PAR_PROTO_HIF
