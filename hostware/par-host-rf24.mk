ifndef ($(par.rf24.spi),)
  def+=PAR_RF24_SPI_DEV=$(par.rf24.spi)
endif

ifndef ($(par.rf24.ce),)
  def+=PAR_RF24_CE_PIN=$(par.rf24.ce)
endif

def+=PAR_RF24_SHA_HASH=1
