#include "par-proto.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void usage(const char *name){
  fprintf(stderr, "usage:\n");
  fprintf(stderr, "  %s <uri> list\n", name);
  fprintf(stderr, "  %s <uri> get <name>\n", name);
  fprintf(stderr, "  %s <uri> set <name> <value>\n", name);
  fprintf(stderr, "  %s <uri> load\n", name);
  fprintf(stderr, "  %s <uri> save\n", name);
  fprintf(stderr, "<uri> - paired device uri. For example:\n");
  fprintf(stderr, "  usb:param-control\n  rf24:00ff00ffab\n");
}

#define tryop(op) {                                                   \
    par_ret_t ret = (op);                                             \
    if(PAR_OK != ret){                                                \
      fprintf(stderr, "%s: %s\n", argv[0], par_ret_str(dev, ret));    \
      par_del(dev);                                                   \
      return ret;                                                     \
    }                                                                 \
  }

int main(int argc, char **argv){
  par_dev_t *dev;
  
  if(argc < 3){
    usage(argv[0]);
    exit(1);
  }
  
  dev = par_new();
  
  tryop(par_open(dev, argv[1]));
  
  if(strcasecmp(argv[2], "list") == 0){
    const par_inf_t *info;
    par_val_t len = par_list(dev, &info), par;
    
    if(info != NULL && len > 0){
      for(par = 0; par < len; par++){
        fprintf(stdout, "%s %s // %s\n", info[par].type, info[par].name, info[par].hint);
      }
    }
  }
  
  if(strcasecmp(argv[2], "get") == 0){
    if(argc == 4){
      par_idx_t par = par_find(dev, argv[3]);
      if(par == PAR_PROTO_NO_PAR){
        fprintf(stderr, "Parameter with name \"%s\" not exists.", argv[3]);
        return 3;
      }
      char *val;
      tryop(par_get_str(dev, par, &val));
      fprintf(stdout, "%s\n", val);
    }
  }
  
  if(strcasecmp(argv[2], "set") == 0){
    if(argc >= 4){
      par_idx_t par = par_find(dev, argv[3]);
      if(par == PAR_PROTO_NO_PAR){
        fprintf(stderr, "Parameter with name \"%s\" not exists.", argv[3]);
        return 3;
      }
      if(argc == 5){ // set
        tryop(par_set_str(dev, par, argv[4]));
      }else{ // reset to default
        tryop(par_def(dev, par));
      }
    }
  }
  
  if(strcasecmp(argv[2], "save") == 0){
    tryop(par_save(dev));
  }
  
  if(strcasecmp(argv[2], "load") == 0){
    tryop(par_load(dev));
  }
  
  par_close(dev);
  par_del(dev);
  
  return 0;
}
