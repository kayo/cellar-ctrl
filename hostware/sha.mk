ifeq ($(sha),on)
  sha.path=sha

  def+=SHA_INTERN=1

  #sha.1?=asm
  #sha.256?=asm

  ifeq ($(sha.1),c)
    sha.obj+=sha1.o
  endif
  ifeq ($(sha.1),asm)
    sha.obj+=sha1-asm.o
  endif

  ifeq ($(sha.224),c)
    sha.obj+=sha224.o sha2_small_common.o
  endif

  ifeq ($(sha.256),c)
    sha.obj+=sha256.o sha2_small_common.o
  endif
  ifeq ($(sha.256),asm)
    sha.obj+=sha256-asm.o
  endif

  ifeq ($(sha.384),c)
    sha.obj+=sha384.o sha2_large_common.o
  endif

  ifeq ($(sha.512),c)
    sha.obj+=sha512.o sha2_large_common.o
  endif

  cc.dir+=$(sha.path)
  obj+=$(addprefix $(sha.path)/,$(sha.obj))
endif
