install:
	getent group dpc-access || groupadd -r dpc-access
	systemctl stop dpc || true
	cp dpcd.elf /usr/sbin/dpcd
	cp org.illumium.dpc.conf /etc/dbus-1/system.d/
	systemctl reload dbus
	cp dpc.service /etc/systemd/system/
	[ -f /etc/dpc.json ] || cp dpc.json /etc/
	systemctl --system daemon-reload
	systemctl enable dpc
	systemctl start dpc

uninstall:
	systemctl stop dpc
	systemctl disable dpc
	rm -f /usr/sbin/dpcd
	rm -f /etc/dbus-1/system.d/org.illumium.dpc.conf
	systemctl reload dbus
	rm -f /etc/system.d/system/dpc.service
#	rm -f /etc/dpc.json
	systemctl --system daemon-reload
