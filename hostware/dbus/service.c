#include "service.h"

#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <sys/time.h>

static int service_introspect(DBusConnection *connection, DBusMessage *message, void *userdata){
  service_t *service = (service_t*)userdata;
  
  DBusMessage *reply = message_helper_return(&service->introspect, message);
  
  message_helper_i11n_iface(DBUS_INTERFACE_INTROSPECTABLE);
  
  message_helper_i11n_method("Introspect");
  message_helper_i11n_method_arg("xml", "s", MESSAGE_OUTPUT);
  message_helper_i11n_method(NULL);
  
  message_helper_i11n_iface(NULL);

  message_helper_i11n_iface(DBUS_INTERFACE_DPC_SERVICE);
  
  message_helper_i11n_method("GetName");
  message_helper_i11n_method_arg("name", "s", MESSAGE_OUTPUT);
  message_helper_i11n_method(NULL);
  
  message_helper_i11n_method("GetVersion");
  message_helper_i11n_method_arg("version", "sai", MESSAGE_OUTPUT);
  message_helper_i11n_method(NULL);
  
  message_helper_i11n_iface(NULL);
  
  for(int i = 0; i < service->count; i++){
    message_helper_i11n_node(service->instances[i].name);
  }
  
  const char *i11n = message_helper_i11n();
  
  dbus_message_append_args(reply, DBUS_TYPE_STRING, &i11n, DBUS_TYPE_INVALID);
  message_helper_output(connection, reply);
  
  return 1;
}

static int service_get_name(DBusConnection *connection, DBusMessage *message, void *userdata){
  service_t *service = (service_t*)userdata;
  
  DBusMessage *reply = message_helper_return(&service->get_name, message);
  
  dbus_message_append_args(reply, DBUS_TYPE_STRING, &service->name, DBUS_TYPE_INVALID);
  message_helper_output(connection, reply);
  
  return 1;
}

static int service_get_version(DBusConnection *connection, DBusMessage *message, void *userdata){
  service_t *service = (service_t*)userdata;
  
  DBusMessage *reply = message_helper_return(&service->get_version, message);
  
  const char *version_string = VERSION_STRING;
  dbus_int32_t version[] = { VERSION };
  dbus_int32_t *version_number = version;

  dbus_message_append_args(reply,
                           DBUS_TYPE_STRING, &version_string,
                           DBUS_TYPE_ARRAY, DBUS_TYPE_INT32, &version_number, sizeof(version)/sizeof(version[0]),
                           DBUS_TYPE_INVALID);
  message_helper_output(connection, reply);
  
  return 1;
}

void service_init(service_t *service){
  service->name = PROJECT;
  
  service->instances = NULL;
  service->count = 0;
  
  message_helper_init(&service->introspect,
                      DBUS_SERVICE_DPC,
                      MESSAGE_METHOD,
                      "/",
                      DBUS_INTERFACE_INTROSPECTABLE,
                      "Introspect",
                      service_introspect,
                      service);
  
  message_helper_init(&service->get_name,
                      DBUS_SERVICE_DPC,
                      MESSAGE_METHOD,
                      "/",
                      DBUS_INTERFACE_DPC_SERVICE,
                      "GetName",
                      service_get_name,
                      service);
  
  message_helper_init(&service->get_version,
                      DBUS_SERVICE_DPC,
                      MESSAGE_METHOD,
                      "/",
                      DBUS_INTERFACE_DPC_SERVICE,
                      "GetVersion",
                      service_get_version,
                      service);
}

void service_dest(service_t *service){
  service_setup(service, NULL);
  if(service->name && strcmp(service->name, PROJECT) != 0){
    free(service->name);
    service->name = NULL;
  }
}

void service_update(service_t *service, DBusConnection *connection){
  struct timeval tv;
  
  if(gettimeofday(&tv, NULL) == 0){
    uint64_t timestamp = tv.tv_sec * 1000 + tv.tv_usec / 1000;
    
    for(int i = 0; i < service->count; i++){
      instance_update(&service->instances[i], connection, timestamp);
    }
  }
}

void service_attach(service_t *service, DBusConnection *connection){
  message_helper_attach(&service->introspect, connection);
  message_helper_attach(&service->get_name, connection);
  message_helper_attach(&service->get_version, connection);
  
  for(int i = 0; i < service->count; i++){
    instance_attach(&service->instances[i], connection);
  }
}

void service_detach(service_t *service, DBusConnection *connection){
  for(int i = 0; i < service->count; i++){
    instance_detach(&service->instances[i], connection);
  }
  
  message_helper_detach(&service->introspect, connection);
  message_helper_detach(&service->get_name, connection);
  message_helper_detach(&service->get_version, connection);
}

int service_setup(service_t *service, json_object *devices){
  if(service->instances && service->count){
    syslog(LOG_INFO, "Delete Instances");
    
    for(int i = 0; i < service->count; i++){
      instance_dest(&service->instances[i]);
    }
    free(service->instances);
    service->instances = NULL;
    service->count = 0;
  }
  
  if(devices){
    if(json_object_is_type(devices, json_type_object)){
      service->count = json_object_object_length(devices);
      
      if(service->count > 0){
        syslog(LOG_INFO, "Create Instances");
        
        service->instances = calloc(service->count, sizeof(instance_t));
        
        int i = 0;
        
        json_object_object_foreach(devices, name, opts){
          const char *uri = NULL;
          int32_t poll_delay = DPC_POLL_DELAY;
          int32_t max_errors = DPC_MAX_ERRORS;
          int32_t rest_delay = DPC_REST_DELAY;
          int32_t test_delay = DPC_TEST_DELAY;
          
          json_object_object_foreach(opts, key, val){
            if((strcmp("uri", key) == 0 ||
                strcmp("resource", key) == 0) &&
               json_object_is_type(val, json_type_string)){
              uri = json_object_get_string(val);
            }else if((strcmp("poll_delay", key) == 0 ||
                      strcmp("polling", key) == 0) &&
                     json_object_is_type(val, json_type_int)){
              poll_delay = json_object_get_int(val);
            }else if((strcmp("max_errors", key) == 0 ||
                      strcmp("maxerrs", key) == 0) &&
                     json_object_is_type(val, json_type_int)){
              max_errors = json_object_get_int(val);
            }else if((strcmp("rest_delay", key) == 0 ||
                      strcmp("reopen", key) == 0) &&
                     json_object_is_type(val, json_type_int)){
              rest_delay = json_object_get_int(val);
            }else if((strcmp("test_delay", key) == 0 ||
                      strcmp("testing", key) == 0) &&
                     json_object_is_type(val, json_type_int)){
              rest_delay = json_object_get_int(val);
            }
          }
          
          instance_init(&service->instances[i++], name, uri,
                        poll_delay, max_errors, rest_delay, test_delay);
        }
      }
    }
  }
  
  return service->count;
}
