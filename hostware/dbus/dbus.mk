dbus.consts?=shared

ifeq ($(dbus.consts),shared)
  def+=_DBUS_CONSTS_SHARED=1

  ifneq ($(dbus.service),)
    def+=DBUS_SERVICES='$(call parsespec,$(dbus.service))'
  endif

  ifneq ($(dbus.interface),)
    def+=DBUS_INTERFACES='$(call parsespec,$(dbus.interface))'
  endif

  ifneq ($(dbus.error),)
    def+=DBUS_ERRORS='$(call parsespec,$(dbus.error))'
  endif
endif

ifeq ($(dbus.consts),static)
  ifneq ($(dbus.service),)
    def+=$(patsubst %,DBUS_SERVICE_%"',$(subst :,='",$(dbus.service)))
  endif

  ifneq ($(dbus.interface),)
    def+=$(patsubst %,DBUS_INTERFACE_%"',$(subst :,='",$(dbus.interface)))
  endif

  ifneq ($(dbus.error),)
    def+=$(patsubst %,DBUS_ERROR_%"',$(subst :,='",$(dbus.error)))
  endif
endif
