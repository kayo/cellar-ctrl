#ifndef __PARAM_H__
#define __PARAM_H__ "param.h"

#include "par-proto.h"
#include "message.h"

typedef struct param param_t;

#include "instance.h"

struct param {
  const char *name;
  const char *type;
  
  int readable:1;
  int writable:1;
  
  int dbus_type;
  char dbus_sign[2];
  
  instance_t *instance;
  par_idx_t index;
  
  char *path;
  
  message_helper introspect;
  message_helper get_type;
  message_helper get_value;
  message_helper set_value;
  message_helper reset_value;
  message_helper value_changed;
};

void param_init(param_t *param, instance_t *instance, par_idx_t index, const par_inf_t *info);
void param_dest(param_t *param);

void param_attach(param_t *param, DBusConnection *connection);
void param_detach(param_t *param, DBusConnection *connection);

void param_update(param_t *param, DBusConnection *connection);

#endif//__PARAM_H__
