#include "dbus.h"

#ifdef _DBUS_CONSTS_SHARED

#  ifdef DBUS_SERVICES
#    define _(name, value) const char *DBUS_SERVICE_ ## name = #value;
DBUS_SERVICES
#    undef _
#  endif//DBUS_SERVICES

#  ifdef DBUS_INTERFACES
#    define _(name, value) const char *DBUS_INTERFACE_ ## name = #value;
DBUS_INTERFACES
#    undef _
#  endif//DBUS_INTERFACES

#  ifdef DBUS_ERRORS
#    define _(name, value) const char *DBUS_ERROR_ ## name = #value;
DBUS_ERRORS
#    undef _
#  endif//DBUS_ERRORS

#endif//_DBUS_CONSTS_SHARED
