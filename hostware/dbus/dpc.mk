cc.dir+=..
ld.dir+=..
ld.lib+=dpc

def+=DPC_POLL_DELAY=$(if $(dpc.poll_delay),$(dpc.poll_delay),0)
def+=DPC_MAX_ERRORS=$(if $(dpc.max_errors),$(dpc.max_errors),0)
def+=DPC_REST_DELAY=$(if $(dpc.rest_delay),$(dpc.rest_delay),0)
def+=DPC_TEST_DELAY=$(if $(dpc.test_delay),$(dpc.test_delay),0)
