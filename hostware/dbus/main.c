#include <stdlib.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <syslog.h>
#include <json.h>

#include "service.h"

static json_object *config(const char *conf_file, const char **conf_fail){
  int conf_desc = open(conf_file, O_RDONLY);
  if(conf_desc < 0){
    *conf_fail = "Unable to open file";
    return NULL;
  }
  
  struct stat conf_stat;
  fstat(conf_desc, &conf_stat);

  if(conf_stat.st_size == 0){
    close(conf_desc);
    *conf_fail = "File is empty";
    return NULL;
  }
  
  char *conf_data = mmap(0, conf_stat.st_size, PROT_READ, MAP_PRIVATE, conf_desc, 0);
  
  if(conf_data == MAP_FAILED){
    close(conf_desc);
    *conf_fail = "Unable to read file";
    return NULL;
  }
  
  enum json_tokener_error conf_jerr;
  json_object *conf_root = json_tokener_parse_verbose(conf_data, &conf_jerr);
  
  munmap(conf_data, conf_stat.st_size);
  close(conf_desc);
  
  if(conf_jerr != json_tokener_success){
    *conf_fail = json_tokener_error_desc(conf_jerr);
    return NULL;
  }
  
  return conf_root;
}

typedef enum state state_t;
enum state {
  STATE_CONF,
  STATE_INIT,
  STATE_WORK,
  STATE_DONE,
  STATE_FAIL,
  STATE_EXIT
};

static state_t state = STATE_INIT;

static void handler(int signum){
  if(signum == SIGTERM || signum == SIGINT){
    state = STATE_DONE;
  }
  if(signum == SIGUSR1 && state == STATE_WORK){
    state = STATE_CONF;
  }
}

int main(){
  const char *log = getenv("DPC_SYSLOG");
  if(!log || strcmp(log, "only") != 0){
    openlog(PROJECT, LOG_CONS | LOG_PERROR, LOG_DAEMON);
  }
  
  struct sigaction sig_act;
  
  memset(&sig_act, 0, sizeof(sig_act));
  sig_act.sa_handler = handler;
  
  sigemptyset(&sig_act.sa_mask);
  sigaddset(&sig_act.sa_mask, SIGTERM);
  sigaddset(&sig_act.sa_mask, SIGINT);
  sigaddset(&sig_act.sa_mask, SIGUSR1);
  
  sigaction(SIGTERM, &sig_act, NULL);
  sigaction(SIGINT, &sig_act, NULL);
  sigaction(SIGUSR1, &sig_act, NULL);
  
  syslog(LOG_INFO, "StartUp");
  
  int status = EXIT_SUCCESS;
  
  service_t *service = NULL;
  
  DBusError error;
  
  DBusConnection* connection = NULL;
  
  for(; state != STATE_EXIT; ){
    switch(state){
      
    case STATE_CONF: {
      const char* file = getenv("DPC_CONFIG");
      
      if(!file){
        file = "./dpc.json";
      }
      
      syslog(LOG_INFO, "Loading config from file (%s)", file);
      
      const char *fail;
      json_object *root = config(file, &fail);
      
      if(!root){
        syslog(LOG_ERR, "Invalid config (%s)", fail);
        state = connection ? STATE_WORK : STATE_FAIL;
        break;
      }
      
      if(connection){
        service_detach(service, connection);
      }
      
      service_setup(service, root);
      
      json_object_put(root);
      
      if(connection){
        service_attach(service, connection);
      }
      
      state = connection ? STATE_WORK : STATE_INIT;
      break;
    }
    case STATE_INIT: {
      if(!service){
        service = malloc(sizeof(service_t));
        service_init(service);
        
        state = STATE_CONF;
        break;
      }
      
      const char* bus_name = getenv("DPC_BUS");
      DBusBusType bus_type = DBUS_BUS_SESSION;
      
      if(bus_name){
        if(strcasecmp(bus_name, ":system:") == 0){
          bus_type = DBUS_BUS_SYSTEM;
          bus_name = NULL;
        }else if(strcasecmp(bus_name, ":session:") == 0){
          bus_type = DBUS_BUS_SESSION;
          bus_name = NULL;
        }
      }
      
      syslog(LOG_INFO, "Connecting to bus (%s)", bus_name ? bus_name : bus_type == DBUS_BUS_SESSION ? ":session:" : ":system:");
      
      dbus_error_init(&error);
      connection = bus_name ? dbus_connection_open(bus_name, &error) : dbus_bus_get(bus_type, &error);
      
      if(dbus_error_is_set(&error)){
        syslog(LOG_ERR, "Connection failed (%s)", error.message);
        dbus_error_free(&error);
        state = STATE_FAIL;
        break;
      }
      
      if(NULL == connection){
        state = STATE_FAIL;
        break;
      }

      syslog(LOG_INFO, "Registering service (%s)", DBUS_SERVICE_DPC);
      
      dbus_error_init(&error);
      int ret = dbus_bus_request_name(connection, DBUS_SERVICE_DPC, DBUS_NAME_FLAG_REPLACE_EXISTING, &error);
      
      if(dbus_error_is_set(&error)){
        syslog(LOG_ERR, "Registration failed (%s)", error.message);
        dbus_error_free(&error);
        state = STATE_FAIL;
        break;
      }
      
      if(DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER != ret){
        state = STATE_FAIL;
        break;
      }
      
      service_attach(service, connection);
      
      state = STATE_WORK;
      break;
    }
    case STATE_WORK: {
      int conf_sleep = getenv("DPC_SLEEP") ? atoi(getenv("DPC_SLEEP")) : 1000;
      
      for(; state == STATE_WORK; ){
        if(dbus_connection_read_write(connection, 0)){
          for(DBusDispatchStatus status = dbus_connection_get_dispatch_status(connection);
              status == DBUS_DISPATCH_DATA_REMAINS;
              status = dbus_connection_dispatch(connection)){
          }
          usleep(conf_sleep);
          service_update(service, connection);
        }else{
          state = STATE_FAIL;
          break;
        }
      }
      
      break;
    }
    case STATE_FAIL:
      status = EXIT_FAILURE;
      
    case STATE_DONE:
      if(service){
        if(connection){
          service_detach(service, connection);
        }
        
        service_dest(service);
        
        free(service);
      }
      
      if(connection){
        syslog(LOG_INFO, "Disconnecting from bus");
        
        dbus_connection_unref(connection);
      }
      
      state = STATE_EXIT;
      break;
    }
  }
  
  syslog(LOG_INFO, "ShutDown");
  
  return status;
}
