#ifndef __SERVICE_H__
#define __SERVICE_H__ "service.h"

#include "message.h"
#include "instance.h"

#include <json.h>

typedef struct service service_t;
struct service {
  instance_t *instances;
  int count;

  char *name;
  
  message_helper introspect;
  message_helper get_name;
  message_helper get_version;
};

void service_init(service_t *service);
void service_dest(service_t *service);

void service_update(service_t *service, DBusConnection *connection);

void service_attach(service_t *service, DBusConnection *connection);
void service_detach(service_t *service, DBusConnection *connection);

int service_setup(service_t *service, json_object *devices);

#endif//__SERVICE_H__
