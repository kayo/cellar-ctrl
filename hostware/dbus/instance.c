#include "instance.h"

#include <stdlib.h>
#include <string.h>
#include <syslog.h>

const char *STATUS_ONLINE = "online";
const char *STATUS_OFFLINE = "offline";

static void instance_status_changed(instance_t *instance, DBusConnection *connection, const char *message){
  DBusMessage *status = message_helper_create(&instance->status_changed, NULL);
  
  instance->status = message;
  dbus_message_append_args(status, DBUS_TYPE_STRING, &instance->status, DBUS_TYPE_INVALID);
  
  message_helper_output(connection, status);
}

static void instance_close(instance_t *instance, DBusConnection *connection){
  if(!par_online(&instance->device)){
    return;
  }
  
  instance_status_changed(instance, connection, STATUS_OFFLINE);
  
  if(instance->params && instance->count){
    for(int i = 0; i < instance->count; i++){
      param_detach(&instance->params[i], connection);
      param_dest(&instance->params[i]);
    }
    free(instance->params);
    
    instance->params = NULL;
    instance->count = 0;
  }
  
  par_close(&instance->device);
  
  syslog(LOG_INFO, "Instance offline (%s)", instance->name);
}

void instance_error(instance_t *instance, par_ret_t ret){
  instance->error_cnt ++;
  
  syslog(LOG_ERR, "Instance Error (%s)", par_ret_str(&instance->device, ret));
}

static void instance_open(instance_t *instance, DBusConnection *connection){
  if(par_online(&instance->device)){
    return;
  }
  
  syslog(LOG_INFO, "Opening instance (%s) (%s)", instance->name, instance->uri);
  
  par_ret_t ret = par_open(&instance->device, instance->uri);
  
  if(ret){
    //instance_error(instance, ret);
    instance_close(instance, connection);
    return;
  }
  
  const par_inf_t *infos;
  instance->count = par_list(&instance->device, &infos);
  
  if(instance->count > 0){
    instance->params = calloc(instance->count, sizeof(param_t));
    
    for(int i = 0; i < instance->count; i++){
      param_init(&instance->params[i], instance, i, &infos[i]);
      param_attach(&instance->params[i], connection);
    }
    
    instance->poll_micro = instance->poll_delay / instance->count;
  }
  
  instance->error_cnt = 0;
  
  instance_status_changed(instance, connection, STATUS_ONLINE);
  
  syslog(LOG_INFO, "Instance online (%s)", instance->name);
}

static int instance_introspect(DBusConnection *connection, DBusMessage *message, void *userdata){
  instance_t *instance = (instance_t*)userdata;
  
  DBusMessage *reply = message_helper_return(&instance->introspect, message);
  
  message_helper_i11n_iface(DBUS_INTERFACE_INTROSPECTABLE);
  
  message_helper_i11n_method("Introspect");
  message_helper_i11n_method_arg("xml", "s", MESSAGE_OUTPUT);
  message_helper_i11n_method(NULL);
  
  message_helper_i11n_iface(NULL);

  message_helper_i11n_iface(DBUS_INTERFACE_DPC_DEVICE);
  
  message_helper_i11n_method("GetStatus");
  message_helper_i11n_method_arg("status", "s", MESSAGE_OUTPUT);
  message_helper_i11n_method(NULL);
  
  if(par_online(&instance->device)){
    message_helper_i11n_method("LoadParams");
    message_helper_i11n_method(NULL);
    
    message_helper_i11n_method("SaveParams");
    message_helper_i11n_method(NULL);
  }
  
  message_helper_i11n_signal("StatusChanged");
  message_helper_i11n_signal_arg("status", "s");
  message_helper_i11n_signal(NULL);
  
  message_helper_i11n_iface(NULL);

  for(int i = 0; i < instance->count; i++){
    message_helper_i11n_node(instance->params[i].name);
  }
  
  const char *i11n = message_helper_i11n();
  
  dbus_message_append_args(reply, DBUS_TYPE_STRING, &i11n, DBUS_TYPE_INVALID);
  message_helper_output(connection, reply);
  
  return 1;
}

static int instance_get_status(DBusConnection *connection, DBusMessage *message, void *userdata){
  instance_t *instance = (instance_t*)userdata;
  
  DBusMessage *reply = message_helper_return(&instance->get_status, message);
  
  dbus_message_append_args(reply, DBUS_TYPE_STRING, &instance->status, DBUS_TYPE_INVALID);
  message_helper_output(connection, reply);
  
  return 1;
}

static int instance_load_params(DBusConnection *connection, DBusMessage *message, void *userdata){
  instance_t *instance = (instance_t*)userdata;
  
  if(!par_online(&instance->device)){
    message_helper_except(connection, message, DBUS_ERROR_DPC_CONNECTION, "Device is not available");
    return 1;
  }

  par_ret_t ret = par_load(&instance->device);
  
  if(ret){
    instance_error(instance, ret);
    message_helper_except(connection, message, DBUS_ERROR_DPC_CONNECTION, "Loading params failed");
    return 1;
  }
  
  DBusMessage *reply = message_helper_return(&instance->load_params, message);
  message_helper_output(connection, reply);
  
  return 1;
}

static int instance_save_params(DBusConnection *connection, DBusMessage *message, void *userdata){
  instance_t *instance = (instance_t*)userdata;
  
  if(!par_online(&instance->device)){
    message_helper_except(connection, message, DBUS_ERROR_DPC_CONNECTION, "Device is not available");
    return 1;
  }

  par_ret_t ret = par_save(&instance->device);
  
  if(ret){
    instance_error(instance, ret);
    message_helper_except(connection, message, DBUS_ERROR_DPC_CONNECTION, "Saving params failed");
    return 1;
  }
  
  DBusMessage *reply = message_helper_return(&instance->save_params, message);
  message_helper_output(connection, reply);
  
  return 1;
}

void instance_init(instance_t *instance, const char *name, const char* uri, uint32_t poll_delay, uint32_t max_errors, uint32_t rest_delay, uint32_t test_delay){
  instance->name = strdup(name);
  instance->uri = strdup(uri);
  
  instance->poll_delay = poll_delay;
  instance->poll_micro = poll_delay;
  
  instance->max_errors = max_errors;
  instance->rest_delay = rest_delay;

  instance->test_delay = test_delay;
  
  instance->status = STATUS_OFFLINE;
  instance->params = NULL;
  instance->count = 0;
  
  instance->polled_ts = 0;
  instance->polled_id = PAR_PROTO_NO_PAR;

  instance->error_cnt = 0;
  instance->closed_ts = 0;
  
  instance->path = malloc(1 + strlen(name));
  instance->path[0] = '/';
  strcpy(instance->path + 1, name);
  
  message_helper_init(&instance->introspect,
                      DBUS_SERVICE_DPC,
                      MESSAGE_METHOD,
                      instance->path,
                      DBUS_INTERFACE_INTROSPECTABLE,
                      "Introspect",
                      instance_introspect,
                      instance);

  message_helper_init(&instance->get_status,
                      DBUS_SERVICE_DPC,
                      MESSAGE_METHOD,
                      instance->path,
                      DBUS_INTERFACE_DPC_DEVICE,
                      "GetStatus",
                      instance_get_status,
                      instance);

  message_helper_init(&instance->load_params,
                      DBUS_SERVICE_DPC,
                      MESSAGE_METHOD,
                      instance->path,
                      DBUS_INTERFACE_DPC_DEVICE,
                      "LoadParams",
                      instance_load_params,
                      instance);

  message_helper_init(&instance->save_params,
                      DBUS_SERVICE_DPC,
                      MESSAGE_METHOD,
                      instance->path,
                      DBUS_INTERFACE_DPC_DEVICE,
                      "SaveParams",
                      instance_save_params,
                      instance);
  
  message_helper_init(&instance->status_changed,
                      DBUS_SERVICE_DPC,
                      MESSAGE_SIGNAL,
                      instance->path,
                      DBUS_INTERFACE_DPC_DEVICE,
                      "StatusChanged",
                      NULL,
                      instance);
}

void instance_dest(instance_t *instance){
  if(instance->name){
    free(instance->name);
    instance->name = NULL;
  }
  
  if(instance->uri){
    free(instance->uri);
    instance->uri = NULL;
  }
  
  if(instance->path){
    free(instance->path);
    instance->path = NULL;
  }
}

void instance_attach(instance_t *instance, DBusConnection *connection){
  message_helper_attach(&instance->introspect, connection);
  message_helper_attach(&instance->get_status, connection);
  message_helper_attach(&instance->load_params, connection);
  message_helper_attach(&instance->save_params, connection);
  
  instance_open(instance, connection);
}

void instance_detach(instance_t *instance, DBusConnection *connection){
  instance_close(instance, connection);
  
  message_helper_detach(&instance->introspect, connection);
  message_helper_detach(&instance->get_status, connection);
  message_helper_detach(&instance->load_params, connection);
  message_helper_detach(&instance->save_params, connection);
}

char instance_ready(instance_t *instance){
  if(par_online(&instance->device)){
    instance->triggered = 1;
    return 1;
  }
  return 0;
}

void instance_update(instance_t *instance, DBusConnection *connection, uint64_t timestamp){
  if(par_online(&instance->device)){
    if(instance->test_delay){
      if(instance->triggered){
        instance->lastop_ts = timestamp;
        instance->triggered = 0;
      }
      
      if(timestamp - instance->lastop_ts > instance->test_delay){
        par_ret_t ret = par_test(&instance->device);
        
        if(ret){
          instance_error(instance, ret);
        }else{
          instance->lastop_ts = timestamp;
        }
      }
    }
  
    if(instance->max_errors){ // close on errors
      if(instance->error_cnt >= instance->max_errors){
        instance_close(instance, connection);
        instance->closed_ts = timestamp;
        return;
      }
    }
    
    if(instance->poll_delay){ // auto polling values
      if(timestamp - instance->polled_ts > instance->poll_micro){
        if(instance_ready(instance)){
          if(++ instance->polled_id >= instance->count){
            instance->polled_id = 0;
          }
          
          param_update(&instance->params[instance->polled_id], connection);
        }
        instance->polled_ts = timestamp;
      }
    }
  }else{
    //if(instance->closed_ts){ // try reopen when closed
    if(timestamp - instance->closed_ts > instance->rest_delay){
      instance_open(instance, connection);
      
      if(par_online(&instance->device)){
        instance->closed_ts = 0;
      }else{
        instance->closed_ts = timestamp;
      }
    }
    //}
  }
}
