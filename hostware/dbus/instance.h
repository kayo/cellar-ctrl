#ifndef __INSTANCE_H__
#define __INSTANCE_H__ "instance.h"

#include "par-proto.h"
#include "message.h"

typedef struct instance instance_t;

#include "param.h"

struct instance {
  char *name; // Instance name
  char *uri;  // Device identifier (ex. usb:param-control, rf24:0f0f0f0f1e@76*password)
  
  uint32_t poll_delay; // Delay (ms) between polling of values from device (0 - polling disabled)
  uint32_t poll_micro; // poll_delay / count
  
  uint32_t max_errors; // If errors more than this value, device will be closed
  uint32_t rest_delay; // Delay (ms) between last closing of device and auto reconnect attemption

  uint32_t test_delay; // Test delay
  
  param_t *params;
  par_len_t count;

  const char *status;
  
  uint64_t polled_ts; // Last polling timestamp
  par_idx_t polled_id;// Last polled parameter
  
  uint32_t error_cnt;
  uint64_t closed_ts; // Last closing timestamp

  char triggered;     // Operation triggered
  uint64_t lastop_ts; // Last operation time
  
  par_dev_t device;
  
  char *path;
  message_helper introspect;
  message_helper get_status;
  message_helper load_params;
  message_helper save_params;
  message_helper status_changed;
};

void instance_init(instance_t *instance, const char *name, const char* uri,
                   uint32_t poll_delay, uint32_t max_errors, uint32_t rest_delay, uint32_t test_delay);
void instance_dest(instance_t *instance);

void instance_attach(instance_t *instance, DBusConnection *connection);
void instance_detach(instance_t *instance, DBusConnection *connection);

void instance_update(instance_t *instance, DBusConnection *connection, uint64_t timestamp);

void instance_error(instance_t *instance, par_ret_t ret);
char instance_ready(instance_t *instance);

#endif//__INSTANCE_H__
