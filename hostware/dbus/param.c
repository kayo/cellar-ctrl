#include "param.h"

#include <stdlib.h>
#include <string.h>
#include <syslog.h>

static int param_introspect(DBusConnection *connection, DBusMessage *message, void *userdata){
  param_t *param = (param_t*)userdata;
  
  DBusMessage *reply = message_helper_return(&param->introspect, message);
  
  message_helper_i11n_iface(DBUS_INTERFACE_INTROSPECTABLE);
  
  message_helper_i11n_method("Introspect");
  message_helper_i11n_method_arg("xml", "s", MESSAGE_OUTPUT);
  message_helper_i11n_method(NULL);
  
  message_helper_i11n_iface(NULL);
  
  message_helper_i11n_iface(DBUS_INTERFACE_DPC_PARAM);
  
  message_helper_i11n_method("GetType");
  message_helper_i11n_method_arg("type", "s", MESSAGE_OUTPUT);
  message_helper_i11n_method(NULL);
  
  if(param->readable){
    message_helper_i11n_method("GetValue");
    message_helper_i11n_method_arg("value", param->dbus_sign, MESSAGE_OUTPUT);
    message_helper_i11n_method(NULL);
  }
  
  if(param->writable){
    message_helper_i11n_method("SetValue");
    message_helper_i11n_method_arg("value", param->dbus_sign, MESSAGE_INPUT);
    message_helper_i11n_method(NULL);
    
    message_helper_i11n_method("ResetValue");
    message_helper_i11n_method(NULL);
  }
  
  if(param->readable && param->instance->poll_delay){
    message_helper_i11n_signal("ValueChanged");
    message_helper_i11n_signal_arg("value", param->dbus_sign);
    message_helper_i11n_signal(NULL);
  }
  
  message_helper_i11n_iface(NULL);
  
  const char *i11n = message_helper_i11n();
  
  dbus_message_append_args(reply, DBUS_TYPE_STRING, &i11n, DBUS_TYPE_INVALID);
  message_helper_output(connection, reply);
  
  return 1;
}

static int param_get_type(DBusConnection *connection, DBusMessage *message, void *userdata){
  param_t *param = (param_t*)userdata;
  
  DBusMessage *reply = message_helper_return(&param->get_type, message);
  
  dbus_message_append_args(reply, DBUS_TYPE_STRING, &param->type, DBUS_TYPE_INVALID);
  message_helper_output(connection, reply);
  
  return 1;
}

static int param_ctype_to_dbus(const char *type){
  if(strstr(type, "int") != NULL){
    uint8_t bits = 32;
    // skip not number
    const char *bs = type;
    for(; bs[0] != '\0' && (bs[0] < '0' || bs[0] > '9'); bs++);
    
    if(bs[0] != '\0'){
      bits = atoi(bs);
    }
    
    if(type[0] == 'u' || type[0] == 'U'){
      switch(bits){
      case 8:  return DBUS_TYPE_BYTE;
      case 16: return DBUS_TYPE_UINT16;
      case 32: return DBUS_TYPE_UINT32;
      case 64: return DBUS_TYPE_UINT64;
      }
    }else{
      switch(bits){
      case 8:  return DBUS_TYPE_BYTE;
      case 16: return DBUS_TYPE_INT16;
      case 32: return DBUS_TYPE_INT32;
      case 64: return DBUS_TYPE_INT64;
      }
    }
    
    return DBUS_TYPE_INT32;
    
  }else if(strstr(type, "bool") != NULL){
    return DBUS_TYPE_BOOLEAN;
    
  }else if(strstr(type, "float") != NULL ||
           strstr(type, "double") != NULL){
    return DBUS_TYPE_DOUBLE;
  }else if(strstr(type, "char") != NULL ||
           strstr(type, "str") != NULL){
    return DBUS_TYPE_STRING;
  }
  
  return DBUS_TYPE_INVALID;
}

static inline dbus_bool_t param_message_append_value(DBusMessage *message, int type, const par_raw_t *value){
  return dbus_message_append_args(message, type, value, DBUS_TYPE_INVALID);
}

static inline dbus_bool_t param_message_extract_value(DBusMessage *message, int type, par_raw_t *value){
  return dbus_message_get_args(message, NULL, type, value, DBUS_TYPE_INVALID);
}

static int param_get_value(DBusConnection *connection, DBusMessage *message, void *userdata){
  param_t *param = (param_t*)userdata;
  
  if(!instance_ready(param->instance)){
    message_helper_except(connection, message, DBUS_ERROR_DPC_CONNECTION, "Parameter is not available");
    return 1;
  }
  
  par_len_t len;
  par_raw_t *val;
  par_ret_t ret = par_get_raw(&param->instance->device, param->index, &val, &len);
  
  if(ret){
    instance_error(param->instance, ret);
    message_helper_except(connection, message, DBUS_ERROR_DPC_CONNECTION, "Getting value failed");
    return 1;
  }
  
  DBusMessage *reply = message_helper_return(&param->get_value, message);
  
  if(param_message_append_value(reply, param->dbus_type, val)){
    message_helper_output(connection, reply);
  }else{
    message_helper_delete(reply);
    message_helper_except(connection, message, DBUS_ERROR_DPC_VALUE, "Value is invalid");
  }
  
  return 1;
}

static int param_set_value(DBusConnection *connection, DBusMessage *message, void *userdata){
  param_t *param = (param_t*)userdata;
  
  if(!instance_ready(param->instance)){
    message_helper_except(connection, message, DBUS_ERROR_DPC_CONNECTION, "Parameter is not available");
    return 1;
  }
  
  par_val_t val[32];
  
  if(!param_message_extract_value(message, param->dbus_type, val)){
    message_helper_except(connection, message, DBUS_ERROR_DPC_VALUE, "Value is invalid");
    return 1;
  }

  par_ret_t ret = par_set_raw(&param->instance->device, param->index, &val, sizeof(val));

  if(ret){
    instance_error(param->instance, ret);
    message_helper_except(connection, message, DBUS_ERROR_DPC_CONNECTION, "Setting value failed");
    return 1;
  }
  
  DBusMessage *reply = message_helper_return(&param->set_value, message);
  
  message_helper_output(connection, reply);
  
  return 1;
}

static int param_reset_value(DBusConnection *connection, DBusMessage *message, void *userdata){
  param_t *param = (param_t*)userdata;
  
  if(!instance_ready(param->instance)){
    message_helper_except(connection, message, DBUS_ERROR_DPC_CONNECTION, "Param is not available");
    return 1;
  }

  par_ret_t ret = par_reset(&param->instance->device, param->index);
  
  if(ret){
    instance_error(param->instance, ret);
    message_helper_except(connection, message, DBUS_ERROR_DPC_CONNECTION, "Resetting value failed");
    return 1;
  }
  
  DBusMessage *reply = message_helper_return(&param->reset_value, message);
  message_helper_output(connection, reply);
  
  return 1;
}

void param_init(param_t *param, instance_t *instance, par_idx_t index, const par_inf_t *info){
  param->index = index;
  
  param->name = info->name;
  param->type = info->type;
  
  if(info->hint && strlen(info->hint) > 0){
    param->readable = strchr(info->hint, 'r') != NULL ? 1 : 0;
    param->writable = strchr(info->hint, 'w') != NULL ? 1 : 0;
  }else{
    param->readable = 1;
    param->writable = 1;
  }
  
  param->dbus_type = param_ctype_to_dbus(info->type);
  param->dbus_sign[0] = ((char)param->dbus_type);
  param->dbus_sign[1] = '\0';
  
  param->instance = instance;
  
  int path_offset = strlen(instance->path);
  param->path = malloc(path_offset + 1 + strlen(info->name));
  strcpy(param->path, instance->path);
  param->path[path_offset] = '/';
  strcpy(param->path + path_offset + 1, info->name);
  
  message_helper_init(&param->introspect,
                      DBUS_SERVICE_DPC,
                      MESSAGE_METHOD,
                      param->path,
                      DBUS_INTERFACE_INTROSPECTABLE,
                      "Introspect",
                      param_introspect,
                      param);

  message_helper_init(&param->get_type,
                      DBUS_SERVICE_DPC,
                      MESSAGE_METHOD,
                      param->path,
                      DBUS_INTERFACE_DPC_PARAM,
                      "GetType",
                      param_get_type,
                      param);

  if(param->readable){
    message_helper_init(&param->get_value,
                        DBUS_SERVICE_DPC,
                        MESSAGE_METHOD,
                        param->path,
                        DBUS_INTERFACE_DPC_PARAM,
                        "GetValue",
                        param_get_value,
                        param);
  }
  
  if(param->writable){
    message_helper_init(&param->set_value,
                        DBUS_SERVICE_DPC,
                        MESSAGE_METHOD,
                        param->path,
                        DBUS_INTERFACE_DPC_PARAM,
                        "SetValue",
                        param_set_value,
                        param);
    
    message_helper_init(&param->reset_value,
                        DBUS_SERVICE_DPC,
                        MESSAGE_METHOD,
                        param->path,
                        DBUS_INTERFACE_DPC_PARAM,
                        "ResetValue",
                        param_reset_value,
                        param);
  }

  if(param->readable && param->instance->poll_delay){
    message_helper_init(&param->value_changed,
                        DBUS_SERVICE_DPC,
                        MESSAGE_SIGNAL,
                        param->path,
                        DBUS_INTERFACE_DPC_PARAM,
                        "ValueChanged",
                        NULL,
                        param);
  }
}

void param_dest(param_t *param){
  param->name = NULL;
  param->type = NULL;
  
  if(param->path){
    free(param->path);
    param->path = NULL;
  }
}

void param_attach(param_t *param, DBusConnection *connection){
  message_helper_attach(&param->introspect, connection);
  message_helper_attach(&param->get_type, connection);

  if(param->readable){
    message_helper_attach(&param->get_value, connection);
  }
  
  if(param->writable){
    message_helper_attach(&param->set_value, connection);
    message_helper_attach(&param->reset_value, connection);
  }
}

void param_detach(param_t *param, DBusConnection *connection){
  message_helper_detach(&param->introspect, connection);
  message_helper_detach(&param->get_type, connection);
  
  if(param->readable){
    message_helper_detach(&param->get_value, connection);
  }

  if(param->writable){
    message_helper_detach(&param->set_value, connection);
    message_helper_detach(&param->reset_value, connection);
  }
}

void param_update(param_t *param, DBusConnection *connection){
  if(param->readable){
    par_len_t len;
    par_raw_t *val;
    par_ret_t ret = par_get_raw(&param->instance->device, param->index, &val, &len);
    
    if(ret){
      instance_error(param->instance, ret);
    }
    
    DBusMessage *event = message_helper_create(&param->value_changed, NULL);
    
    if(param_message_append_value(event, param->dbus_type, val)){
      message_helper_output(connection, event);
    }else{
      message_helper_delete(event);
    }
  }
}
