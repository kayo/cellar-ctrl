#ifdef __AVR__
#  include <avr/pgmspace.h>
#else
#  define PROGMEM
#  define pgm_read_byte(ptr) (*(ptr))
#  define pgm_read_dword(ptr) (*(ptr))
#  define memcpy_P memcpy
#endif
